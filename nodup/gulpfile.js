var gulp = require('gulp');
// var plumber = require('gulp-plumber');
// var rename = require('gulp-rename');
// var babel = require('gulp-babel');
// var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
// var imagemin = require('gulp-imagemin');
// var cache = require('gulp-cache');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');


gulp.task('browserify', function() {
    return  browserify({
        entries: ['./src/js/index.js'],
        transform: [
            ['babelify', {
                "presets": ['es2015'],
                "compact": false
            }
            ],
            ['browserify-css']
        ],
        debug: true
    }).require("./src/js/index.js", {expose: "nodup"}).on('error', e => console.log(e))
        .bundle().on('error', e => console.log(e))
        .pipe(source('nodup.js'))
        .pipe(buffer())
        // .pipe(uglify())
        .pipe(gulp.dest('dist'));

    // browserify({debug: true, standalone: 'nodup'}).on('error', function(e){console.log(e);})
    //     .transform("babelify", { presets: ["es2015", "react"] }).on('error', function(e){console.log(e);})
    //
    //     .require("./src/js/index.js", {expose: "nodup"}).on('error', function(e){console.log(e);})
    //     .bundle().on('error', function(e){console.log(e);})
    //     .pipe(source('nodup.js'))
    //     .pipe(buffer())
    //     // .pipe(uglify())
    //     .pipe(gulp.dest('dist'));
});


gulp.task('css', function() {
    return gulp.src('src/css/styles.scss')
        .pipe(sass().on('error', sass.logError))
        // .pipe(gulp.dest('dist'));
});

gulp.task('default', ['css', 'browserify' ], function(){
    gulp.watch('src/**/*', ['css', 'browserify']);
});