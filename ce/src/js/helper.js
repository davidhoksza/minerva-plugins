const LOG = 1;

function sortAlphabetic(a, b) {
    aname = a.toLowerCase();
    bname = b.toLowerCase();
    if (aname < bname) return -1;
    if (aname > bname) return 1;
    return 0;
}

function isString(obj) {
    return (Object.prototype.toString.call(obj) === '[object String]');
}

function assert(condition, message) {
    if (!condition) {
        message = message || "Assertion failed";
        if (typeof Error !== "undefined") {
            throw new Error(message);
        }
        throw message; // Fallback
    }
}

/*
 Tests equality of two object with respect to given array of keys. If no are given, all common keys are tested.
 If the keys are compound objects and descend is set to true, they are also tested for equality,
 but only on common attributes, i.e. keys are not taken into account in such case.
 */
function objectsEquality(o1, o2, keys, descend) {
    if(descend === undefined) descend = true;

    if (keys === undefined) {
        keys = Object.keys(o1).filter(function (d) {
            return new Set(Object.keys(o2)).has(d);
        });
    }
    for (let k = 0; k < keys.length; k++) {
        if (typeof(o1[keys[k]]) === "object" &&  typeof(o2[keys[k]]) === "object") {
            if (o1[keys[k]] === null || o2[keys[k]] === null) {
                //nulls are also objects
                if (o1[keys[k]] !== null || o2[keys[k]] !== null) return false;
            }
            else if (descend && !objectsEquality(o1[keys[k]], o2[keys[k]])) return false;
        }
        else if (typeof(o1[keys[k]]) != typeof(o2[keys[k]])) return false;
        else if (o1[keys[k]] != o2[keys[k]]) return false;

    }
    return true;
}
/*
 objectsEquality({a:1},{a:1}) -> True
 objectsEquality({a:1, b:1},{a:1}) -> True
 objectsEquality({a:1, b:1},{a:1, b:2}) -> false
 objectsEquality({a:1, b:1},{a:1, b:2}, ["a"]) -> True
 objectsEquality({a:1, b:1},{a:1, b:2}, ["a", "b"]) -> false
 objectsEquality({a:1, b:1, c:1},{a:1, b:2}, ["a", "c"]) -> false
 objectsEquality({a:1, b:1, c:null},{a:1, b:2}, ["a", "c"]) -> false
 objectsEquality({a:1, b:1, c:null},{a:1, b:2, c:null}, ["a", "c"]) -> true //ALTHOUGH NULL==NULL shoould lead to NULL, not true
 objectsEquality({a:1, b:1, c:null},{a:1, b:2, c:7}, ["a", "c"]) -> false
 objectsEquality({a:1, b:1, c:null},{a:1, b:1, c:7}, ["a", "b"]) -> true
 objectsEquality({a:1, b:1, c:null, d:{a:1, b:2}},{a:1, b:1, c:7, d:{a:1, b:2}}, ["a", "b", "d"]) -> true
 objectsEquality({a:1, b:1, c:null, d:{a:1, b:2}},{a:1, b:1, c:7, d:{a:1, b:3}}, ["a", "b", "d"]) -> false
 objectsEquality({a:1, b:1, c:null, d:{a:1, b:2}},{a:1, b:1, c:7, d:{a:1, b:3}}, ["a", "b", "d"], false) -> true
 */

/*
 Removes duplicities from an array. Elements are duplicit if the agree in values of keys
 (the can disagree in the remaining values). If such a situation occurs, only the first of the occurrences is kept.
 */
function uniquify (arr, keys) {
    for (let i = arr.length-1; i>=0; i--) {
        for (let j = i -1 ; j >= 0; j--) {
            // if (JSON.stringify(arr[i]) == JSON.stringify(arr[j])) {
            if (objectsEquality(arr[i], arr[j], keys)) {
                arr.splice(i, 1);
                break;
            }
        }
    }
}
// a=[{a:1, b:2, c:3}, {a:1, b:2, c:4}, {a:2, b:2, c:3}, {a:1, b:2, c:4}]; uniquify(a, ["a","b"]); a
// a=[{a:1, b:2, c:3}, {a:1, b:2, c:4}, {a:2, b:2, c:3}, {a:1, b:2, c:4}]; uniquify(a, ["a","b", "c"]); a
// a=[{a:1, b:2, c:3}, {a:1, b:2, c:4}, {a:2, b:2, c:3}, {a:1, b:2, c:4}]; uniquify(a, ["b"]); a
// a=[{a:1, b:{b1:1, b1:2}, c:3}, {a:1, b:{b1:1, b1:2}, c:4}, {a:2, b:{b1:1, b1:2}, c:3}, {a:1, b:{b1:1, b1:2}, c:4}]; uniquify(a, ["b"]); a -> 1 object
// a=[{a:1, b:{b1:1, b1:2}, c:3}, {a:1, b:{b1:1, b1:2}, c:4}, {a:2, b:{b1:1, b1:2}, c:3}, {a:1, b:{b1:1, b1:3}, c:4}]; uniquify(a, ["b"]); a -> 2 object

function objectInArray(o, arr, keys) {
    for (let i = 0; i < arr.length; i++)
        if (objectsEquality(o, arr[i], keys)) return i;
    return -1;
}
// objectInArray({a:1,b:2,c:3, d:4}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}], ["a"])
// objectInArray({a:1,b:2,c:3, d:4}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}], ["d"])
// objectInArray({a:1,b:2,c:3, d:4}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}], ["a", "b"])
// objectInArray({a:1,b:2,c:3, d:{e:1, b:2}}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}, {a:3, d:{e:1, b:2}}], ["d"]) -> 3
//objectInArray({a:1,b:2,c:3, d:{e:1, b:2}}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}, {a:3, d:{e:1, b:3}}], ["d"]) -> -1



function roughSizeOfObject(object) {

    let objectList = [];
    let stack = [object];
    let bytes = 0;

    while (stack.length) {
        let value = stack.pop();

        if (typeof value === 'boolean') {
            bytes += 4;
        }
        else if (typeof value === 'string') {
            bytes += value.length * 2;
        }
        else if (typeof value === 'number') {
            bytes += 8;
        }
        else if
        (
            typeof value === 'object'
            && objectList.indexOf(value) === -1
        ) {
            objectList.push(value);

            for (let i in value) {
                stack.push(value[i]);
            }
        }
    }
    return bytes;
}

function renameProperty(o, old_key, new_key) {
    if (old_key !== new_key) {
        Object.defineProperty(o, new_key,
            Object.getOwnPropertyDescriptor(o, old_key));
        delete o[old_key];
    }
}

function log() {
    if (LOG == 1) console.log.apply(null, arguments)
}

module.exports = {
    sortAlphabetic: sortAlphabetic,
    isString: isString,
    assert: assert,
    uniquify: uniquify,
    objectInArray: objectInArray,
    objectsEquality: objectsEquality,
    roughSizeOfObject: roughSizeOfObject,
    renameProperty: renameProperty,
    log: log

};