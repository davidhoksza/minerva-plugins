(function(ce, $, undefined ) {

    /*********************
     Initialization of constants and settings
     *********************/

    const pluginPath = "http://localhost:8080/minerva-plugins/ce/";
    const LOG = 0;
    const PDB_MAPPING_FROM_MINERVA = 1;

    var speciesTypes = [];
    var reactionTypes = [];
    var compartmentTypes = [];

//    var GROUP_BY_TYPES = ["species", "compartment", "reaction"]
    var GROUP_BY_TYPES = ["compartment", "reaction"];
    const PATH_EQUALITY_ATTRS = ["name", "_type", /*"role", "reactionType",*/ "_compartmentId", "_complexId"] //To be used for filtering out already encountered species
    var color20c;

    // var graph = new graphlib.Graph({multigraph: true});

    const consts = {
        marginTop: 0,
        marginRight: 10,
        marginBottom: 0,
        marginLeft: 10,
        treeMarginRight: 10,
        nodeCircleSize: 10,
        nodeSpacingVertical: 2,
        d3TreeHeightCorrection: 0, //TODO - for some reason, the tree coordinates overflow given height when too many items are present -> decreasing height by a constant make the tree to fit the view
        treeLevelWidth: 130, //number of pixels taken by one level of the tree in the visualization
        highlightedNodeOpacity: 0.3,
        highlightedNodeBorder: 3,
        highlightedNodeBorderOpacity: 1,

        compartmentTypeName: "compartment"
    };

    const settings = {
        neighboursBySurface: true, //If true, encountered elements are highlighted by area. If false, by icon.
        maxNameLength: 15
    };

    // consts.width = parseInt($("#ceExplorationDiv").css("width")) - consts.marginRight - consts.marginLeft;
    consts.height = $(window).height() -  - consts.marginTop - consts.marginBottom;
    consts.nodeTextOffsetX = consts.nodeCircleSize + 10;

    consts.markerWidth = Math.min(50, consts.treeLevelWidth / 2); //TODO test this
    consts.markerHeight = consts.nodeCircleSize;
    consts.markerPaddingRight = 2;
    consts.markerColor = "#ccc";

    var zoom, drag, diagonal, svg, svgG;
    var treeRoots = [];
    var d3Tree;

    var cnt_nodes = 0;
    var duration = 500;

    var svgCR;

    var highligtedObjects = [];

    /*********************
     Page structure
     *********************/

    function initPageStructure() {
        // $("#minervaAppDiv").wrap('<div style="heighfvt:100%"></div>');
        $("#minervaAppDiv").css("overflow", "hidden");
        $("#minervaAppDiv").css("width", "");

        $("#minervaAppDiv").before('<div id="ceExplorationDiv"></div>');
        $("#ceExplorationDiv").after('<div id="ceSplitBarDiv"></div>');

        $('#ceSplitBarDiv').mousedown(function (e) {
            e.preventDefault();
            $(document).mousemove(function (e) {
                e.preventDefault();
//            console.log(e.pageX ); return;
                var x = $("body").width() - e.pageX;
//            if (x > min && x < max && e.pageX < ($(window).width() - mainmin)) {
                $('#ceExplorationDiv').css("width", x);
//                $('#div2').css("margin-left", x);
//            }
            })
        });
        $(document).mouseup(function (e) {
            $('button.headerHideButton').trigger('click');
            $(document).unbind('mousemove');
            $('button.headerHideButton').trigger('click');
        });

        $('button.headerHideButton').trigger('click');

        document.getElementById("ceExplorationDiv").innerHTML =
            '<div id="ceTooltip" class="hidden"></div>' +
            '<div id="ceNodeMenu" class="hidden"></div>' +
            '<div id="cePathMenu" class="hidden"></div>' +
            '<div id="ceMenu" class="hidden"></div>' +
            // '<div id="ceLoading" class="hidden"><ul class="bokeh"><li></li><li></li><li></li></ul></div>' +
            '<div id="ceStructureViewer" class="ce-structure-viewer">' +
            '   <a href="javascript:void(0)" class="ce-closebtn" onclick="ce.closeSV()">&times;</a>' +
            '   <div class="ce-records-list"></div>' +
            '   <div id="ceLitemolApp"></div>' +
            '   <script src="js/litemol.js"></script>' +
            '</div>' +
            '<div class="modal fade" id="ceModal" role="dialog">' +
            '   <div class="modal-dialog">' +
            '       <div class="modal-content">' +
            '       </div>' +
            '   </div>' +
            '</div>' +
            '<div class="container-fluid" >' +
            '   <div id="rowOptions" class="row">' +
            '        <div class="col-md-2">' +
            '           <button id="ceGoButton" type="button" class="btn btn-block btn-xs btn-success disabled" style="width:100%" onclick="ce.go()">GO</button>' +
            '        </div>' +
            '        <div class="col-md-2">' +
            '           <button id="ceLocator" type="button" class="btn btn-block btn-xs btn-danger" onclick="ce.locator()"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>' +
            '        </div>' +
            '        <div class="col-md-8" onmouseleave="$(\'#ceOptionsForm\').hide(\'fast\');">' +
            '           <button id="ceOptionsShowHideButton" type="button" class="btn btn-xs btn-primary" onclick="ce.toggler(\'ceOptionsForm\')">Options</button>'+
            '            <form id="ceOptionsForm" class ="ce-hidden">' +
            '                <div class="form-group">' +
            '                    <label for="ceSelSpeciesType">Species types:</label>' +
            '                    <select multiple class="form-control" id="ceSelSpeciesType" onchange="ce.reinitializeVisualization()">' +
            '                    </select>' +
            '                   <div class="checkbox">' +
            '                       <label>' +
            '                           <input type="checkbox" id="ceLocateOnExpansion"> Locate on expansion' +
            '                       </label>' +
            '                   </div>' +
            '                   <div class="input-group">' +
            '                       <input type="number" id="ceBacktrackMaxDepth" class="form-control" min="1">' +
            '                       <span class="input-group-addon">Backtrack depth</span>' +
            '                   </div>' +
            '                </div>' +
            '                <div class="form-inline">' +
            '                   <label class="radio-inline">' +
            '                       <input type="radio" name="inlineRadioOptions" id="highlightRadioIcon" onclick="ce.setHighlightBySurface(false);"> Highlight by icon' +
            '                   </label>' +
            '                   <label class="radio-inline">' +
            '                       <input type="radio" name="inlineRadioOptions" id="highlightRadioSurface" onclick="ce.setHighlightBySurface(true);"> Highlight by surface' +
            '                   </label>' +
            '                </div>' +
            // '                <button id="ceOptionsExport" type="button" class="btn btn-xs" onclick="ce.export()">Export</button>'+
            '            </form>' +
            '        </div>' +
            '    </div>' +
            '   <div class="row">' +
            '        <div class="col-md-12">' +
            '           <div id="ceLegend"></div>' +
            '        </div>' +
            '   </div>' +
            '</div>' +
            '<div id="ceChart">' +
            '    <svg>' +
            '        <defs>' +
            '            <filter x="0" y="0" width="1" height="1" id="solid">' +
            '                <feFlood flood-color="yellow"/>' +
            '                <feComposite in="SourceGraphic"/>' +
            '            </filter>' +
            '           <radialGradient id="myRadialGradient4" fx="5%" fy="5%" r="65%" spreadMethod="pad">' +
            '               <stop offset="0%"   stop-color="#00ee00" stop-opacity="1"/>' +
            '               <stop offset="100%" stop-color="#006600" stop-opacity="1" />' +
            '           </radialGradient>' +
            '        </defs>' +
            '    </svg>' +
            '</div>' +
            '<div id="ceSearchBarContainers" class="container-fluid"> '+
            '   <div class="row no-gutter"> '+
            '       <div class="col-md-5"> '+
            '          <div class="ce-search-bar-container">' +
            '              <input type="text" id="ceInputSearchSpeciesName" class="form-control"  placeholder="Species name" >' +
            '          </div>' +
            '       </div>' +
            '       <div class="col-md-5"> '+
            '          <div class="ce-search-bar-container">' +
            '              <input type="text" id="ceInputSearchReactionType" class="form-control"  placeholder="Reaction type" >' +
            '          </div>' +
            '       </div>' +
            '       <div class="col-md-1"> '+
            '          <div class="ce-search-bar-container">' +
            '              <input type="text" class="form-control" >' +
            '                  <button class="btn search" type="submit">' +
            '                      <span class="glyphicon glyphicon-search"></span>' +
            '                  </button>' +
            '          </div>' +
            '       </div>' +
            '       <div class="col-md-1"> '+
            '          <div class="ce-search-bar-container last">' +
            '              <input type="text" class="form-control" >' +
            '                  <button class="btn export" type="text">' +
            '                      <span class="glyphicon glyphicon glyphicon-download-alt"></span>' +
            '                  </button>' +
            '          </div>' +
            '       </div>' +
            '   </div>' +
            '</div>';

        $(".ce-search-bar-container button.search").on("click", searchFilterUpdate);
        $("#ceInputSearchSpeciesName,#ceInputSearchSpeciesName").on("keydown", function(e){
            if(e.keyCode == 13) searchFilterUpdate();
        });
        $(".ce-search-bar-container button.export").on("click", function(){ce.export()});

        if (settings.neighboursBySurface) $("#highlightRadioSurface").prop("checked", true);
        else $("#highlightRadioIcon").prop("checked", true);

        return customMap.getConfiguration().then(function(conf) {
            //Fill in the types
            conf._elementTypes.sort().forEach(function (type) {
                $("#ceSelSpeciesType").append('<option val="' + type + '">' + type + '</option>');
            });
            $("#ceSelSpeciesType").attr("size", conf._elementTypes.length)
            //By default check protein
            $("#ceSelSpeciesType").val(["Protein", "Complex"])

            $('button.headerHideButton').trigger('click'); //TODO - remove after interface to google maps resize available
        });
    }

    function createMarkers() {
        let nodeCircleOffset = getSvgClassStrokeWidth("ce-node") + consts.nodeCircleSize / 2;
        let linkStrokeWidth = getSvgClassStrokeWidth("ce-link");

        let circleR = consts.markerHeight * 0.95 / 2;
        let arrowWidth = consts.markerHeight;
        let dotR = consts.markerHeight * 0.1;
        let dotSpacing = consts.markerWidth / 2 / 4; //3 dots spanning half of the marker

        let markerVerticalLineHeight = consts.nodeCircleSize;
        let markerVerticalLineStrokeWidth = linkStrokeWidth * 2;

        let centerVertical = consts.markerHeight / 2;
        let centerHorizontal = consts.markerWidth / 2;

        defs = d3.select("#ceChart svg defs");


        /* ARROW */
        var x1 = consts.markerWidth - consts.markerPaddingRight - consts.markerHeight, y1 = 0;
        var x2 = consts.markerWidth - consts.markerPaddingRight, y2 = centerVertical;
        var x3 = consts.markerWidth - consts.markerPaddingRight - consts.markerHeight, y3 = consts.markerHeight;
        var markerArrowProduct = defs.append('marker').attr("id", "ceMarkerArrowProduct");
        markerArrowProduct.append("line")
            .attr("x1", 0)
            .attr("y1", function () { return y2;} )
            .attr("x2", function () { return x1;} )
            .attr("y2", function () { return y2;} )
            .style("stroke", consts.markerColor)
            .style("stroke-width", linkStrokeWidth);
        markerArrowProduct.append("path")
            .attr("d",
                " M " + x1 + "," + y1 +
                " L " + x2 + "," + y2 +
                " L " + x3 + "," + y3 +
                " L " + x1 + "," + y1)
            .style("fill", consts.markerColor);

        /* BACK ARROW */
        var x1 = centerHorizontal, y1 = centerVertical;
        var x2 = centerHorizontal + arrowWidth, y2 = 0;
        var x3 = centerHorizontal + arrowWidth, y3 = consts.markerHeight;
        var markerArrowReactant = defs.append('marker').attr("id", "ceMarkerArrowReactant");
        markerArrowReactant.append("line")
            .attr("x1", function () { return x2;} )
            .attr("y1", function () { return y1;} )
            .attr("x2", function () { return consts.markerWidth - consts.markerPaddingRight;} )
            .attr("y2", function () { return y1;} )
            .style("stroke", consts.markerColor)
            .style("stroke-width", linkStrokeWidth);
        for (let i = 1; i <= 3; i++){
            markerArrowReactant.append("circle")
                .attr("cx", function () { return dotSpacing * i;} )
                .attr("cy", centerVertical )
                .attr("r", function () { return dotR; })
                .style("fill", consts.markerColor);
        }
        markerArrowReactant.append("path")
            .attr("d",
                " M " + x1 + "," + y1 +
                " L " + x2 + "," + y2 +
                " L " + x3 + "," + y3 +
                " L " + x1 + "," + y1)
            .style("fill", consts.markerColor);

        /* VERTICAL */
        let xVertical = consts.markerWidth * 0.9;
        let markerVerticalProduct = defs.append('marker').attr("id", "ceMarkerVerticalProduct");
        markerVerticalProduct.append("line")
            .attr("x1", 0)
            .attr("y1", centerVertical)
            .attr("x2", xVertical)
            .attr("y2", centerVertical)
            .style("stroke", consts.markerColor)
            .style("stroke-width", linkStrokeWidth);
        markerVerticalProduct.append("line")
            .attr("x1", xVertical )
            .attr("y1", 0)
            .attr("x2", xVertical )
            .attr("y2", consts.markerHeight)
            .style("stroke", consts.markerColor)
            .style("stroke-width", markerVerticalLineStrokeWidth);

        /* BACK VERTICAL */
        var markerVerticalReactant = defs.append('marker').attr("id", "ceMarkerVerticalReactant");
        markerVerticalReactant.append("line")
            .attr("x1", centerHorizontal )
            .attr("y1", centerVertical )
            .attr("x2", function () { return consts.markerWidth - consts.markerPaddingRight;} )
            .attr("y2", centerVertical )
            .style("stroke", consts.markerColor)
            .style("stroke-width", linkStrokeWidth);
        markerVerticalReactant.append("line")
            .attr("x1", centerHorizontal )
            .attr("y1", 0)
            .attr("x2", centerHorizontal )
            .attr("y2", consts.markerHeight)
            .style("stroke", consts.markerColor)
            .style("stroke-width", markerVerticalLineStrokeWidth);
        for (let i = 1; i <= 3; i++){
            markerVerticalReactant.append("circle")
                .attr("cx", function () { return dotSpacing * i;} )
                .attr("cy", centerVertical )
                .attr("r", function () { return dotR; })
                .style("fill", consts.markerColor);
        }

        /* CIRCLE */
        var cx = consts.markerWidth - consts.markerHeight / 2 - consts.markerPaddingRight;
        var cy = consts.markerHeight/2;
        var markerCircle = defs.append('marker').attr("id", "ceMarkerCircle");
        markerCircle.append("line")
            .attr("x1", 0)
            .attr("y1", function () { return cy;} )
            .attr("x2", function () { return cx;} )
            .attr("y2", function () { return cy;} )
            .style("stroke", consts.markerColor)
            .style("stroke-width", linkStrokeWidth);
        markerCircle.append("circle")
            .attr("cx", function () { return cx;} )
            .attr("cy", function () { return cy;} )
            .attr("r", function () { return circleR; })
            .style("fill", consts.markerColor);

        /* ALL MARKERS */

        defs.selectAll("marker")
            .attr("markerWidth", function () {return consts.markerWidth; })
            .attr("markerHeight", function () { return consts.markerHeight; })
            .attr("refX", 0)
            .attr("refY", function () { return consts.markerHeight / 2; })
            .attr("orient", "auto")
            .attr("markerUnits", "userSpaceOnUse")
    }

    /*********************
     Minerva interaction
     *********************/

    // function Cache () {
    //     cache: {}; //modelId->id
    //
    //
    //     this.addBioEntity = function (entityId) {
    //         if (!(entityId._modelId in cache)) cache[entityId._modelId] = {};
    //         if (!(entityId.id in cache[entityId._modelId])) cache[entityId._modelId][entityId.id] = bioentityId;
    //     }
    //
    //     this.getBioEntity = function (bioentityId) {
    //         if (entityId._modelId in cache && entityId.id in cache[entityId._modelId]) return cache[entityId._modelId][entityId.id];
    //         else return undefined;
    //     }
    // }
    // cache = Cache();

    function searchFilterUpdate(){
        let speciesName = $("#ceInputSearchSpeciesName").val();
        let reactionType = $("#ceInputSearchReactionType").val();

        log("filtering by speciesName ", speciesName, "and reactionType", reactionType);

        treeRoots.forEach(function (root) {
            root.eachAfter(function (node) {
                //if any of the children of this node needs is visible than this node needs to be shown as well
                if ("children" in node) {
                    for (let i = 0; i < node.children.length; ++i) {
                        if (node.children[i].show) {
                            node.show = true;
                            return;
                        }
                    }
                }
                if ("bioEntityIds" in node.data) {
                    node.show = true;
                    let bioEntity = node.data.bioEntityIds[0];
                    if (!(
                        (speciesName == "" || bioEntity.getName().toLowerCase().includes(speciesName.toLowerCase()) ) &&
                        (reactionType == "" ||  !("reactionType" in bioEntity) ||
                        bioEntity.reactionType.toLowerCase().includes(reactionType.toLowerCase()) )
                    )){
                        node.show = false;
                    }
                }
                else {
                    //If its compartment and was set to true because of its children than it needs to be hidden
                    node.show = false;
                }
            })
        })

        log("tree roots after application of filters", treeRoots);
        treeRoots.forEach(function(root, i){ updateVisualization(root, i); });
    }


    function searchListener(elements) {
        ce.searchElements = elements;
        if (ce.searchElements.length > 0) { //empty when clicked on white space
            if (ce.searchElements[0].length !== undefined) ce.searchElements = ce.searchElements[0];

            // ce.searchElements.forEach(function (d) {
            //     d.modelId = d._modelId;
            // });
            if (ce.searchElements.length > 0 &&  ce.searchElements[0].constructor.name != "Reaction") {
                $("#ceGoButton").removeClass("disabled");
                return;
            }
        }
        $("#ceGoButton").addClass("disabled");
    }

    ce.go = function go() {
        if ($("#ceGoButton").attr("class").indexOf("disabled") < 0) addRoot(ce.searchElements);
    }

    ce.locator = function locator() {
        let svg = $("#ceChart").find("svg");
        if (svg.attr("class") === undefined || svg.attr("class").indexOf("ce-cursor-glass") < 0) {
            // $("#ceLocator").removeClass("disabled");
            if (svg.attr("class") !== undefined) svg.attr("class", svg.attr("class") + " ce-cursor-glass");
            else svg.attr("class", "ce-cursor-glass");

        }
        else {
            // $("#ceLocator").addClass("disabled");
            $("#ceChart").find("svg").removeClass("ce-cursor-glass");
            svg.attr("class", svg.attr("class").replace("ce-cursor-glass", ""));
        }
    };

    ce.registerPlugin = function(customMap) {

        function includeCSS(cssPath) {
            if (!document.getElementById(cssPath))
            {
                var head  = document.getElementsByTagName('head')[0];
                var link  = document.createElement('link');
                link.id   = cssPath;
                link.rel  = 'stylesheet';
                link.type = 'text/css';
                link.href = cssPath;
                link.media = 'all';
                head.appendChild(link);
            }
        }

        function includeJS(script) {
            return $.ajax({
                url: script,
                dataType: "script",
                success: function () {
                    log(script + " loaded");
                },
                error: function () {
                    throw new Error("Could not load script " + script);
                }
            });
        }

        includeCSS(pluginPath + "css/styles.css");
        includeCSS(pluginPath + "css/LiteMol-plugin.css");



        return Promise.all([
            includeJS(pluginPath + "js/d3.v4.min.js"),
            includeJS(pluginPath + "js/LiteMol-plugin.js"),
        ]).then( function(){
            ce.customMap = customMap; //To be used when querying the map
            customMap.addListener({
                dbOverlayName: "search",
                type: "onSearch",
                callback: searchListener
            });

            return initPageStructure().then(function(){
                createMarkers();
                initVisualization();
            }).then(function(){
                return includeJS(pluginPath + "js/litemol.js");
            })
        });
    };

    function createQuery(bioEntityId) {
        return {
            id :  parseInt(bioEntityId.id),
            modelId : "_modelId" in bioEntityId ? bioEntityId._modelId : bioEntityId.modelId, //can happen when I pass results of search, i.e. the object of type IdentifiedElement
            type : "ALIAS"
        }
    }

    function matchSpeciesType(bioEntity){
        let types = $("#ceSelSpeciesType").val();
        return types.indexOf(bioEntity.getType()) >= 0;
    }

    function fetchBioObject(bioEntityIds, type) {
        let queryArray = []
        for (let i = 0; i < bioEntityIds.length; i++) {
            queryArray.push(createQuery(bioEntityIds[i]));
        }
        // log("queryArray", queryArray);
        let promise;
        if (type=="ALIAS") promise = ce.customMap.getBioEntityById(queryArray);
        else if (type=="REACTION") promise = ce.customMap.getReactionsWithElement(queryArray);
        else assert(false);

        return promise.then(function(results){
            log("results in fetchBioObject", results);
            // return results.filter(filterFunction).map(function(d){
            return results.map(function(d){
                // var jd = JSON.parse(JSON.stringify(d));
                return deepCopy(d);
            })
        }).catch(function(d){
            log("Caught error when fetching bioentities", queryArray);
        });
    }

    // function deepCopyBioObject(o) {
    //     return $.extend( true, {}, o );
    //     // newObj = Object.create(temp1.__proto__);
    //     // var dc = JSON.parse(JSON.stringify(d));
    //     // dc.charge = d.getCharge();
    //     // dc
    //
    // }

    function fetchReactions(bioEntityIds) {
        return fetchBioObject(bioEntityIds, "REACTION");
        // // let promises = [];
        // let queryArray = []
        // for (let i = 0; i < bioEntityIds.length; i++) {
        //     queryArray.push(createQuery(bioEntityIds[i]));
        //     // let q = createQuery(bioEntityIds[i]);
        //     // promises.push(ce.customMap.getReactionsWithElement(q).then(function(result){
        //     //     return JSON.parse(JSON.stringify(result));}));
        // }
        // // console.log("queryArray reactions"); console.log(queryArray);
        // return ce.customMap.getReactionsWithElement(queryArray).then(function(results){
        //     // console.log("results reactions"); console.log(results);
        //     return results.map(function(d){
        //         return JSON.parse(JSON.stringify(d));
        //     })
        // });
        // return promises;
    }
    /*

     */
    function fetchBioEntities(bioEntityIds) {
        return fetchBioObject(bioEntityIds, "ALIAS");
        // // let promises = [];
        // let queryArray = []
        // for (let i = 0; i < bioEntityIds.length; i++) {
        //     queryArray.push(createQuery(bioEntityIds[i]));
        //     // promises.push(ce.customMap.getBioEntityById(q).then(function(result){
        //     //     return JSON.parse(JSON.stringify(result));}));
        // }
        // console.log("queryArray"); console.log(queryArray);
        // return ce.customMap.getBioEntityById(queryArray).then(function(results){
        //     return results.filter(filterFunction).map(function(d){
        //         return JSON.parse(JSON.stringify(d));
        //     })
        // }).catch(function(d){
        //     console.log("Caught error when fetching bioentities");
        //     console.log(queryArray);
        // });

        // return promises;
    }

    /*********************
     Visualization-related routines
     *********************/

    function initVisualization() {
        color20c =  d3.scaleOrdinal(d3.schemeCategory10);
        zoom = d3.zoom()
            .scaleExtent([0.5, 10])
            .on("zoom", zoomed);
        ce.zoom = zoom;

        // drag = d3.drag()
        //     // .origin(function (d) {
        //     //     return d;
        //     // })
        //     .on("start", dragstarted)
        //     .on("drag", dragged)
        //     .on("end", dragended);

        ce.treeRoots = treeRoots;

        svgCR = $("#ceChart svg")[0].getBoundingClientRect();

        // diagonal = d3.svg.diagonal()
        //     .projection(function (d) {
        //         return [d.y, d.x];
        //     }); //moved to updateVisualization


        svg = d3.select("#ceChart svg")
            .attr("id", "ceSvg")
            // .attr("width", "100%")
            // .attr("height", "100%")
            // .attr("height", consts.height + consts.marginTop + consts.marginBottom)
            // .attr("height", consts.height + consts.marginTop + consts.marginBottom)
            .append("g")
            .attr("transform", "translate(" + consts.marginLeft + "," + consts.marginTop + ")")
            // .attr("transform", "translate(" + consts.marginLeft + "," + consts.marginTop + ")")
            .call(zoom);

        var rect = svg.append("rect")
            .attr("id", "ceSvgOverlayRect")
            .on("click", function(d){
                $("#ceNodeMenu").addClass("hidden");
                $("#cePathMenu").addClass("hidden");

                menu = d3.select("#ceMenu");
                if (menu.classed("hidden") && $(".ce-path-link").length > 0) {
                    menu.selectAll("div").remove();
                    menu
                        .classed("hidden", false)
                        .style("left", function(){ return getMouseClickPositionInSvg(d3.event).x  + 16 + "px";})
                        .style("top", function(){ return getMouseClickPositionInSvg(d3.event).y + "px";} )
                        .append("div")
                        .text("CLEAR PATHS")
                        .style("font-size", 10)
                        .on("click", function (d) {
                            clearPaths();
                            highlightInMap();
                            d3.select("#ceMenu").classed("hidden", true);
                        });
                }
                else {
                    menu.classed("hidden", true);
                }
            });

        // svg = svg.append("g");
        svgG = svg.append("g");
        ce.svgG = svgG;

    }

    function getMouseClickPositionInSvg(d3Event) {
        return {
            x: d3Event.pageX - $("#ceChart").offset().left,
            y: d3Event.pageY
        }
    }



    function getSvgClassStrokeWidth(svgClass) {
        assert(svgClass !== undefined);

        $("#ceChart svg").append('<circle class="' + svgClass + '" id="circleForStrokeWidth"></circle>');
        let stroke = parseFloat($("#circleForStrokeWidth").css("stroke-width"));
        $("#circleForStrokeWidth").remove();

        return stroke;
    }

    // d3.select(self.frameElement).style("height", "800px");

    function addRoot(entities, level) {
        if(level === undefined) level = 0;

        let query;
        if (typeof entities === "string") query = entities;
        else query = entities[0].name;
        minerva.ServerConnector.getElementsByQuery({query: query, perfectMatch: true}).then(function (result) {
            return fetchBioEntities(result);
        }).then(function(entities){
            log("entities", entities);

            assert(typeof entities === "object" && entities.length > 0);

            removeRootsFrom(level);

            if (level === 0) {
                cleanLegends();
            }

            log("d3 for entities", entities[0]);
            treeRoots.push(d3.hierarchy({
                name: entities[0].getName(),
                _modelId: entities[0].getModelId(),
                children: [],
                type: entities[0].getType(),
                compartment: "",
                // reactionType: "",
                bioEntityIds: entities
            }));

            /*
             * d3Tree needs to be reintialized each time for situations where nodes would not fit on screen.
             * In such a case, the height (or actually width before transofrmation) will be increased to fit
             * the number of nodes so that they do not overlap.
             */
            // d3Tree = d3.tree()
            // // .nodeSize([30,])
            // // .separation(function separation(a, b) {
            // //       return a.parent == b.parent ? 2 : 1;
            // // })
            //     .size([getTreesMaxHeight(treeRoots), 0]); //
            // .size([20, 0]);
            // size([consts.height, 0])]; //TODO
            // d3Tree[0].nodeSize(10);

            // treeRoots[treeRoots.length - 1].x0 = treeRoots.length == 1 ? consts.height / 2 : treeRoots[treeRoots.length - 2].x0;
            // treeRoots[treeRoots.length - 1].y0 = 0;
            treeRoots[treeRoots.length - 1].x0 =
                (treeRoots.length === 1 ? consts.height / 2 - consts.d3TreeHeightCorrection : treeRoots[treeRoots.length - 2].x0);
            treeRoots[treeRoots.length - 1].y0 = 0;

//        svg.selectAll("g.node").remove(); //TODO
            //Update needs to be done for all roots, since they might need to change color of their nodes, for example
            // for (let i = treeRoots.length-1; i >= 0; i--) updateVisualization(treeRoots[i], i);
            treeRoots.forEach(function(root, i){
                let highlight = (i == treeRoots.length - 1 ? true : false);
                updateVisualization(root, i, highlight);
            });
            // updateVisualization(treeRoots[treeRoots.length - 1], treeRoots.length - 1);
            if (treeRoots.length == 1) {
                resetSvgPosition();
            }
            else {
                focusSvgOnPosition([getTreePosRight(treeRoots.length - 2) - consts.treeLevelWidth, 0])
            }

            if (treeRoots.length > 1) {
                findMatchingNodesInTree(treeRoots.length - 2);
            }

            locateInMap(treeRoots[treeRoots.length - 1]);
        });
    }

    function findMatchingNodesInTree(ixTree) {
        assert(ixTree >= 0);
        if (ixTree >= treeRoots.length - 1) return;

        let node = treeRoots[ixTree + 1]
        $("#ceTree" + ixTree + " .ce-node").each(function(){
            let circle = $(this).find("circle");
            var datum = d3.select(this).datum();
            if ("bioEntityIds" in datum.data && objectsEquality(datum.data, node.data, ["name", "_modelId"])) {
                circle.css("stroke", "");
                d3.select(this).classed("ce-spawned", true);
            }
            else {
                d3.select(this).classed("ce-spawned", false);
                circle.css("stroke", function(d){return getNodeColor(node, this);})
            }
        });
    }

    function removeRoot(ix) {

        $("#ceTree" + ix).remove();
        treeRoots.splice(ix, 1)
    }

    function removeRootsFrom(ixRoot) {
        for (ix = treeRoots.length - 1; ix >= ixRoot; ix--) {
            removeRoot(ix);
            $("#ceLegendTreeRootname"+ixRoot).remove();
        }
    }

    function getMaxNumberOfNodesPerLevel(t){
        let cntNodes = [];
        for (let i = 0; i <= t.height; i++) {
            cntNodes.push(0)
        }
        t.each(function(n){
            cntNodes[n.depth]++;
        });
        return Math.max.apply(null,cntNodes);
    }

    function getTreesMaxHeight(tr) {
        let maxCnts = [];
        for (let i = 0; i < tr.length; i++) {
            maxCnts.push(getMaxNumberOfNodesPerLevel(treeRoots[i]));
        }
        let maxCnt = Math.max.apply(null,maxCnts);
        // console.log(maxCnt);

        // return svgCR.bottom - svgCR.top - consts.marginTop - consts.marginBottom - consts.d3TreeHeightCorrection;

        return Math.max(
            svgCR.bottom - svgCR.top - consts.marginTop - consts.marginBottom - consts.d3TreeHeightCorrection,
            maxCnt * consts.nodeCircleSize + (maxCnt - 1) * consts.nodeSpacingVertical);

    }

    function updateVisualization(source, ixTree, highlight) {
        if(highlight === undefined) highlight = true;
        log("tree root in updateVisualization", source);

        // Compute the new tree layout.
        // var nodes = d3Tree[0].nodes(treeRoots[ixTree]).reverse(),
        //     links = d3Tree[0].links(nodes);
        d3Tree = d3.tree().size([getTreesMaxHeight(treeRoots), 0]); //the max size can change with every change, so this needs to reinitialized every time

        d3Tree(treeRoots[ixTree]);
        var links = treeRoots[ixTree].links(); //tree adds x and y coordinates + results in object on which links function is called
        var nodes = treeRoots[ixTree].descendants(); //gets all the objects which now have x and y coordinates

        // Normalize for fixed-depth.
        nodes.forEach(function (d) {
            d.y = d.depth * consts.treeLevelWidth + consts.nodeCircleSize / 2;
        });

        var treeSvg = svgG.select("#ceTree" + parseInt(ixTree));
        if (treeSvg.empty()) {
            treeSvg = svgG.append("g")
                .attr("id", "ceTree" + parseInt(ixTree))
                .attr("class", "ce-tree")
                .attr("transform", function() {
                    return "translate(" + getTreePosLeft(ixTree) + ",0)"
                }) //TODO
        }

        var node = treeSvg.selectAll("g.ce-node")
            .data(nodes, function (d) {
//                c = d.compartment.replace(/_/g, " ")
//                rt = d.reactionType.replace(/_/g, " ")
//                if (compartmentTypes.indexOf(c) == -1) compartmentTypes.push(c);
//                if (reactionTypes.indexOf(rt) == -1) reactionTypes.push(rt);
                return d.treeNodeId || (d.treeNodeId = ++cnt_nodes);
            });

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("g")
            .attr("class", "ce-node")
            .attr("transform", function () {
                return "translate(" + source.y0 + "," + source.x0 + ")";
            })
            .on("mouseover", function(d){
                if ($("#ceChart").find("svg").attr("class") !== undefined &&
                    $("#ceChart").find("svg").attr("class").indexOf("ce-cursor-glass")>=0) {

                    locateInMap(d, true);
                }
            });

        nodeEnter.append("circle")
            .attr("r", 1e-6)
            .on("click", nodeClick)
            .on('mouseover', function (d) {
                d3.select("#ceTooltip")
                    .html(function () {
                        var output = '';
                        str = '<div class="ce-tooltip-species">' + d.data.name + "</div>";
                        if (d.depth > 0 && "bioEntityIds" in d.data) {
                            str += '<div class="ce-tooltip-bioentities">Bioentities:</div>';
                            d.data.bioEntityIds.forEach(function (bioEntity) {
                                str +=
                                    '<div class="ce-tooltip-bioentity">' +
                                    '   <div>Id: <span class="ce-tooltip-bioentity-id">' + bioEntity.id + '</span></div>' +
                                    '   <div>Role: <span class="ce-tooltip-bioentity-role">' + bioEntity.role + '</span></div>' +
                                    '   <div>Raction type: <span class="ce-tooltip-bioentity-reaction-type">' + bioEntity.reactionType + '</span></div>';
                                if (isComplex (bioEntity)) {
                                    //TODO - component decomposition

                                }

                                str += '</div>';
                            });
                        }
                        return str;
                    })
                    .style("left", function(){ return getMouseClickPositionInSvg(d3.event).x  + 16 + "px";})
                    .style("top", function(){ return getMouseClickPositionInSvg(d3.event).y + "px";} )
                d3.select("#ceTooltip").classed("hidden", false);
            })
            .on("mouseout", function () {
                d3.select("#ceTooltip").classed("hidden", true);
            });

        nodeEnter.append("text")
        //            .attr("x", function(d) { return d.children || d._children ? -15 : 15; })
            .attr("x", function () {
                return consts.nodeTextOffsetX;
            })
            .attr("class", function(d){
                let className = "ce-species";
                if ("bioEntityIds" in d.data && isComplex(d.data.bioEntityIds[0])) {
                    className += " complex";
                }
                return className;
            })
            .attr("dy", ".35em")
            //            .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "begin"; })
            .attr("text-anchor", "begin")
            //            .attr("text-color", "red")
            .text(function (d) {
                return d.data.name.length > settings.maxNameLength ? d.data.name.substring(0, settings.maxNameLength-3)+"..." : d.data.name;
            })
            //            .attr("filter", "url(#solid)")
            .style("fill-opacity", 1e-6);


        // Transition nodes to their new position.
        var nodeUpdate = nodeEnter.merge(node);

        nodeUpdate
            .transition()
            .duration(duration)
            .attr("transform", function (d) {
                return "translate(" + d.y + "," + d.x + ")";
            })
            // .on("end")
            .call(transitionEnd, repositionTrees, ixTree + 1);
//            .call(transitionEnd, updateLinksToRoots);

        nodeUpdate.selectAll("circle")
            .attr("r", consts.nodeCircleSize / 2)
            .style("fill", function(d) { return getNodeColor(d, this);})
            .style("fill-opacity", function(d) { return "show" in d && !d.show ? 0.1 : 1;})
            .style("stroke", function(d) { return getColorForElement(this);})
            .style("stroke-opacity", function(d) { return "show" in d && !d.show ? 0.1 : 1;});

        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function (d) {
                return "translate(" + source.y + "," + source.x + ")";
            })
            .remove()
            .call(transitionEnd, function(){if (highlight) highlightInMap();});

        nodeExit.select("circle")
            .attr("r", 1e-6);

        nodeExit.select("text")
            .style("fill-opacity", 1e-6);

        // Update the links…

        let diagonal = function(d) { //TODO d3.linkHorizontal()
            return "M" + d.source.y + "," + d.source.x
                + "C" + (d.source.y + d.target.y) / 2 + "," + d.source.x
                + " " + (d.source.y + d.target.y) / 2 + "," + d.target.x
                + " " + d.target.y + "," + d.target.x;
        }

        var link = treeSvg.selectAll("path.ce-link")
            .data(links, function (d) {
                return d.target.treeNodeId;
            });

        // Enter any new links at the parent's previous position.
        var linkEnter = link.enter().insert("path", "g")
            .attr("class", "ce-link")
            .attr("marker-end", function (d, i) {
                // if (!("role" in d.target)) return "";
                // else if (d.target.role.toLowerCase() == "product")
                //     return "url(#ceMarkerArrow)";
                // else if (d.target.role.toLowerCase() == "reactant")
                //     return "url(#ceMarkerVertical)";
                // else if (d.target.role.toLowerCase() == "modifier")
                //     return "url(#ceMarkerCircle)";
                // else return "";

                if (!("reactionType" in d.target.data)) return "";
                else {
                    return "url(#" + getMarkerIdFromReaction(d.target.data.reactionType, d.target.data.role) + ")";
                    // let markers = ["ceMarkerArrowReactant", "ceMarkerArrowProduct",
                    //     "ceMarkerVerticalReactant", "ceMarkerVerticalProduct", "ceMarkerCircle"]
                    // // return "url(#" + markers[Math.floor(Math.random() * markers.length)] + ")";
                    // return "url(#" + markers[(i-1)%markers.length] + ")";
                    //
                    // reactionTypeDerived = getDerivedReactionType(d.target.data.reactionType).toLowerCase();
                    // if (reactionTypeDerived  == "positive")
                    //     return "url(#ceMarkerArrow)";
                    // else if (reactionTypeDerived  == "negative")
                    //     return "url(#ceMarkerVertical)";
                    // else if (reactionTypeDerived  == "neutral")
                    //     return "url(#ceMarkerVerticalReactant)";
                    // else return "";
                }
            })
            .attr("d", function (d) {
                var o = {x: source.x0, y: source.y0};
                return diagonal({source: o, target: o});
            })
            .on("click", pathClick);

        // Transition links to their new position.
        var linkUpdate = linkEnter.merge(link);

        let nodeStrokeWidth = getSvgClassStrokeWidth("ce-node");
        linkUpdate.transition()
            .duration(duration)
            .attr("d", function(d) {
                /*
                Change position of the curve end so that it ends before target. The offset size is defined by the
                 marker space. Moving only the marker with refX and refY causes the marker not the follow the direction
                 of the line for big node sizes
                 */
                y = d.target.y;
                if ("bioEntityIds" in d.target.data){
                    y -= (consts.nodeCircleSize / 2 + nodeStrokeWidth);
                    y -= consts.markerWidth;
                }
                var target = {x: d.target.x, y: y};
                return diagonal({source: d.source, target: target});

            })
            .call(transitionEnd, backtrackPath);
            // .each('end', function() {
            //     d3.select(this);
            // });

        // Transition exiting links to the parent's new position.
        var linkExit = link.exit().transition()
            .duration(duration)
            .attr("marker-end", "")
            .attr("d", function (d) {
                var o = {x: source.x, y: source.y};
                return diagonal({source: o, target: o});
            })
            // .each('end', function() {
            //     d3.select(this).attr("marker-end", "");
            // })
            .remove();

        // Stash the old positions for transition.
        nodes.forEach(function (x) {
            x.x0 = x.x;
            x.y0 = x.y;
        });

        updateLegends();
    }

    function getNodeColor(node, e){
        return isExplorationEnd(node) ? "rgba(0,0,0, 0)" : getColorForElement(e);
    }

    function getBioentitiesWindow(bioEntities) {
        var coordsMin = {x: Number.MAX_VALUE, y: Number.MAX_VALUE};
        var coordsMax = {x: Number.MIN_VALUE, y: Number.MIN_VALUE};

        for (let i = 0; i < bioEntities.length; i++){

            if (bioEntities[i].x < coordsMin.x) coordsMin.x = bioEntities[i].x;
            if (bioEntities[i].y < coordsMin.y) coordsMin.y = bioEntities[i].y;
            if (bioEntities[i].x > coordsMax.x) coordsMax.x = bioEntities[i].x;
            if (bioEntities[i].y > coordsMax.y) coordsMax.y = bioEntities[i].y;
        }

        return {x1: coordsMin.x, y1: coordsMin.y, x2: coordsMax.x, y2: coordsMax.y}
    }

    function locateInMap(node, subtree){
        if(subtree === undefined) subtree = false;

        assert(subtree || "bioEntityIds" in node.data);

        function getBioentitiesUpToLeaves(node){ //TODO D3
            let be = [];
            if ("bioEntityIds" in node.data) be = node.data.bioEntityIds;
            if (nodeHasVisibleChildren(node.data)) {
                for (let i = 0; i < node.children.length; i++) {
                    be = be.concat(getBioentitiesUpToLeaves(node.children[i]))
                }
            }
            return be;
        }

        let bioEntityIds = subtree ? getBioentitiesUpToLeaves(node) : node.data.bioEntityIds;

        if (bioEntityIds.length > 0) {
            coords = getBioentitiesWindow(bioEntityIds);

            ce.customMap.fitBounds({
                modelId: bioEntityIds[0]._modelId,
                x1: coords.x1,
                y1: coords.y1,
                x2: coords.x2,
                y2: coords.y1
            });
        }
    }

    function getColorForTreeIx(ix) {
        return color20c(treeRoots.length - 1 - ix); //the color of the last tree will be always the same

    }

    function getColorForElement(e) {
        return getColorForTreeIx(getParentTreeIx(e));
    }

    function highlightPath(el) {
        node = el.__data__;
        ixTree = getParentTreeIx(el);

        let path = [];
        while("parent" in node) {
            path = path.concat(node);
            node = node.parent;
        }
        path = path.concat(node);

        // ce.customMap.getHighlightedBioEntities().then(function(highlighted) {
        //     let toShow = [];
        //     let toHide = [];
        //
        //     highligtedIds = highlighted.map(function(d) {
        //         let type = d.element.constructor.name == "Alias" ? "ALIAS" : "REACTION";
        //         return {
        //             element: {id: d.element.id, modelId: d.element._modelId, type: type},
        //             type: d.type,
        //             options: {}
        //         }
        //     });
        //
        //     path.forEach(function(bioEntity){
        //
        //     })
        //
        // }
    }

    function highlightInMap(){
        log("highlight in map");

         ce.customMap.getHighlightedBioEntities().then(function(highlighted) {

             highligtedIds = highlighted.map(function(d) {
                 let type = d.element.constructor.name == "Alias" ? "ALIAS" : "REACTION";
                 return {
                     element: {id: d.element.getId(), modelId: d.element.getModelId(), type: type},
                     type: d.type,
                     options: deepCopy(d.options)
                 }
             });

             var toShow = [];
             var toKeep = [];
             //If a path is present in the tree, only that will be highligted
             if ($(".ce-path-link").length > 0) {
                 log("path highlight")
                 $(".ce-path-link").each(function() {
                     let d = getD3Datum(this);
                     if ("bioEntityIds" in d.target.data) { //by highlighting only targets, we avoid higlighting all the root entities
                         for (let i = 0; i < d.target.data.bioEntityIds.length; i++) {
                             let dd = d.target.data.bioEntityIds[i];
                             let e = {
                                 element: {id: dd.id, modelId: dd._modelId, type: "ALIAS"},
                                 type : "ICON"
                             };
                             var ix = objectInArray(e, highligtedIds)
                             if (ix < 0) toShow.push(e); else toKeep.push(ix);

                             if ("reaction" in dd) {
                                 let e = {
                                     element: {id: dd.reaction.id, modelId: dd._modelId, type: "REACTION"},
                                     type: "SURFACE",
                                     options: {
                                         color: "#cc0000",
                                     }
                                 };
                                 var ix = objectInArray(e, highligtedIds)
                                 log("is reaction to highlight in highligtedids?", e, highligtedIds, ix)
                                 if (ix < 0) toShow.push(e); else toKeep.push(ix);
                             }
                         }
                     }
                 });
             } else {

                 $(".ce-node").each(function () {
                     let d = getD3Datum(this);
                     if ("bioEntityIds" in d.data) {
                         // ceSpecies.push(d);
                         for (let i = 0; i < d.data.bioEntityIds.length; i++) {
                             let dd = d.data.bioEntityIds[i];
                             let e = {
                                 element: {id: dd.id, modelId: dd._modelId, type: "ALIAS"},
                                 // type: settings.neighboursBySurface || d.depth == 0 ? "SURFACE" : "ICON",
                                 options: {
                                     color: getColorForTreeIx(treeRoots.length-1),
                                     opacity: consts.highlightedNodeOpacity,
                                     lineColor: getColorForTreeIx(treeRoots.length-1),
                                     lineWeight: consts.highlightedNodeBorder,
                                     lineOpacity: consts.highlightedNodeBorderOpacity
                                     // color: getColorForElement($(this)[0])
                                 }
                             };
                             if (settings.neighboursBySurface) {
                                 e.type = (d.depth == 0 ? "ICON" : "SURFACE");
                             }
                             else {
                                 e.type = (d.depth == 0 ? "SURFACE" : "ICON");
                             }
                             var ix = objectInArray(e, highligtedIds)
                             if (ix < 0) toShow.push(e); else toKeep.push(ix);

                             if ("reaction" in dd) {
                                 let e = {
                                     element: {id: dd.reaction.id, modelId: dd._modelId, type: "REACTION"},
                                     type: "SURFACE",
                                     options: {
                                         // color: getColorForElement($(this)[0])
                                         color: getColorForTreeIx(treeRoots.length-1)
                                     }
                                 };
                                 var ix = objectInArray(e, highligtedIds)
                                 if (ix < 0) toShow.push(e); else toKeep.push(ix);
                             }
                         }
                     }
                 });
             }


             var toHide = [];
             for (let i = 0; i < highligtedIds.length; i++) {
                 if (toKeep.indexOf(i) <0 && objectInArray(highligtedIds[i], toShow) < 0) toHide.push(highligtedIds[i]);
             }

             // console.log("start of highlighting");
             // console.log("toShow"); console.log(toShow);
             // console.log("toHide"); console.log(toHide);
             ce.customMap.hideBioEntity(toHide).then(function (d) {
                 ce.customMap.showBioEntity(toShow);
             });
             // console.log("end of highlighting");
         });

    }

    function isExplorationEnd(node) {
        return /*node.depth > 0 && */ "isLeaf" in node; //TODO
        // return !(node.depth == 0 ||
        // (Object.keys(node).indexOf("children") >= 0 && node.children.length > 0) ||
        // (Object.keys(node).indexOf("_children") >= 0 && node._children.length > 0));
    }

    function transitionEnd(transition, callback, attr) {
        var n;
        if (transition.empty()) {
            callback(attr);
        }
        else {
            n = transition.size();
            transition.on("end", function () {
                n--;
                if (n === 0) {
                    callback(attr);
                }
            });
        }
    }

//     function getNodeNameByGroupByIx(nodeData, groupByIx) {
//         let groupByTexts = []
//         for (ix = 1; ix <= 3; ix++) groupByTexts.push($("#ceSelGroupBy" + ix + " :selected").text().toLowerCase())
//
// //        if (groupByTexts[groupByIx].startsWith(GROUP_BY_TYPES[0])) return nodeData.w;
//         if (groupByTexts[groupByIx].startsWith(GROUP_BY_TYPES[0])) return nodeData.attrs.c2;
//         else if (groupByTexts[groupByIx].startsWith(GROUP_BY_TYPES[1])) return nodeData.attrs.t;
//         return "bollocks";
//     }

    function getParentTreeIx(e) {
        return parseInt($(e).parents("g.ce-tree").prop("id").replace("ceTree", ""));
    }

    function pathClick(path) {
        // menu = d3.select("#cePathMenu");
        // console.log("pat clicked");
        //
        //
        // if (d3.select(this).classed(".ce-path-link")) {
        //     console.log("path with link clicked");
        //     menu = d3.select("#cePathMenu");
        //     menu.selectAll("div").remove();
        //     menu.append("div")
        //         .text("CLEAR PATH")
        //         .style("font-size", 10)
        //         .on("click", function(d){
        //             d3.select("#cePathMenu").classed("hidden", true);
        //             clearPaths();
        //         })
        //         .style("left", d3.event.pageX + 16 + "px")
        //         .style("top", d3.event.pageY + "px")
        //         .classed("hidden", false);
        // }

    }

    function nodeClick(node) {

        var parentG =  this.parentNode;

        var linkTexts = []
        if (!("bioEntityIds" in node.data)) {
            linkTexts = [["EXPAND/COLAPSE", "exp"], ["LOCATE SUBTREE", "locate_subtree"]];
        }
        else {
            if (node.depth == 0) linkTexts = [["EXPAND/COLLAPSE", "exp"], ["REMOVE ROOT", "despawn"], ["LOCATE", "locate"], ["LOCATE SUBTREE", "locate_subtree"]];
            else if (isExplorationEnd(node)) linkTexts = [["LOCATE", "locate"], ["SPAWN NEW ROOT", "spawn"]];
            else linkTexts = [["EXPAND/COLLAPSE", "exp"], ["SPAWN NEW ROOT", "spawn"], ["LOCATE", "locate"], ["LOCATE SUBTREE", "locate_subtree"]];
            // linkTexts = linkTexts.concat([["HIGHLIGHT PATH", "highlight_path"]])
            linkTexts = linkTexts.concat([["BACKTRACK", "backtrack"], ["SHOW STRUCTURE", "struct"]])
        };

        selMenu = d3.select("#ceNodeMenu");
        if (selMenu.classed("hidden")) {
            selMenu.selectAll("div").remove();
            selMenu.selectAll("div").data(linkTexts)
                .enter()
                .append("div")
                .text(function (d) {
                    return d[0];
                })
                .style("font-size", 10)
                .on("click", function (d) {
                    nodeMenuClick(d[1], parentG);
                });
            selMenu
                .style("left", function(){ return getMouseClickPositionInSvg(d3.event).x  + 16 + "px";})
                .style("top", function(){ return getMouseClickPositionInSvg(d3.event).y + "px";} )
                .classed("hidden", false);
        }
        else selMenu.classed("hidden", true);
    }

    /*
     * If bioentities match in keys, they are merged into one entity. The information about the
     * individiual entities comprising the merged entity is stored in the bioEntity variable.
     */
    function  groupBioEntites(entities, keys) {
        for (let i = entities.length-1; i>=0; i--) {
            for (let j = i - 1; j >= 0; j--) {
                if (objectsEquality(entities[i], entities[j], keys, false)) {
                    entities[j].bioEntityIds = entities[j].bioEntityIds.concat(entities[i].bioEntityIds);
                    entities.splice(i, 1);
                    break;
                }
            }
        }
    }

    /*
     If any of the entities appears in rootPath (with respect to given keys), it will be removed from the list.
     */
    function filterBioEntites(entities, rootPath, keys) {
        for (let i = entities.length - 1; i >= 0; i--) {
            for (let j = 0; j < rootPath.length; j++) {
                match = true;
                for (let k = 0; k < keys.length; k++) {
                    //If key is not in the rootPath object, is is considered as matched (happens in roots which have only type and name)
                    if (Object.keys(rootPath[j]).indexOf(keys[k]) >= 0 && entities[i][keys[k]] != rootPath[j][keys[k]]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    entities.splice(i, 1);
                    break;
                }
            }
        }
    }

    function fetchNeighbours(node) {
        return fetchReactions(node.data.bioEntityIds).then(function(bioEntitiesReactions){
            // cpElementsReactions = JSON.parse(JSON.stringify(elementsReactions));
            // ce.log_reactions = JSON.parse(JSON.stringify(bioEntitiesReactions));
            log("bioEntitiesReactions", deepCopy(bioEntitiesReactions));
            var bioEntities = [];
            for (let i = 0; i < bioEntitiesReactions.length; i++) {
                reaction = bioEntitiesReactions[i];

                ["products", "reactants", "modifiers"].forEach(function(typeName) {
                    let ents = reaction["_"+typeName].map(function (d){
                        let e = d;
                        e.role = typeName.slice(0,-1);
                        e.reactionType = reaction._type;
                        e.reaction = reaction;
                        return e;
                    });
                    // Remove the products, reactants and modifiers to get rid of circular references which are problematic
                    // later when copying objects with JSON.stringify. We are working here over copies of the original
                    // reactions objects, so this does not affect the reactions in the map.
                    delete reaction["_"+typeName];
                    bioEntities = bioEntities.concat(ents);
                });
            }

            //Filter out bioentities not matching selected species type
            log("bioEntities before filtration by type", bioEntities);
            bioEntities = bioEntities.filter(matchSpeciesType);
            log("bioEntities after filtration by type", bioEntities);

            uniquify(bioEntities, ["id", "role"]);
            log("uniquified", bioEntities)

            compartmentIds = bioEntities.map(function(d) {
                return {
                    "id": d.getCompartmentId(),
                    "_modelId": d.getModelId()
                }
            });
            //remove records without _compartmentId
            compartmentIds = compartmentIds.filter(function(d) {
                return d.id !== undefined && d.id !== null;
            });
            log("compartmentIds without nulls", compartmentIds);
            uniquify(compartmentIds, ["id", "_modelId"]);
            log("after uniq compartmentIds", compartmentIds);

            //For each compartment and model we have one representative bioentity
            return fetchBioEntities(compartmentIds).then(function(compartments){
                log("fetched compartments", compartments);
                //Convert compartment into a dictionary
                var dictCompartments = {};
                compartments.forEach(function(d){dictCompartments[d.id] = d;});

                log("bioents to connect to compartments", bioEntities);
                //Attach compartment names to the original bioentities, remove reactants and products and return them
                bioEntities.forEach(function(d){
                    let idAux = d.getCompartmentId();
                    d.comparmentName = idAux ? dictCompartments[idAux].name : "Extracellular";
                });
                log("bioents with compartments", bioEntities);
                return bioEntities;
            });
        });
    }

    function getDerivedReactionType(type){
        if (type.toLowerCase().indexOf("positive") >= 0) return "positive";
        if (type.toLowerCase().indexOf("negative") >= 0) return "negative";
        return "neutral";
    }

    function getMarkerIdFromReaction(reactionName, role){
        role = role.toLowerCase();
        if (role == "modifier") return "ceMarkerCircle";
        else {
            if (
                ['state transition',
                    'known transition omitted',
                    'unknown transition',
                    'transcription',
                    'translation',
                    'transport',
                    'heterodimer association',
                    'heterodimer dissociation',
                    'truncation',
                    'positive influence',
                    'unknown positive influence',
                    'reduced physical stimulation',
                    'unknown reduced physical stimulation',
                    'reduced modulation',
                    'unknown reduced modulation',
                    'reduced trigger',
                    'unknown reduced trigger',
                    'catalysis',
                    'unknown catalysis',
                    'physical stimulation',
                    'modulation',
                    'trigger'].indexOf(reactionName.toLowerCase()) >= 0) {

                return role == "reactant" ? "ceMarkerArrowProduct" : "ceMarkerArrowReactant";
            }
            else return role == "reactant" ? "ceMarkerVerticalProduct" : "ceMarkerVerticalReactant";
        }

        return "";
    }

    function fetchChildren(node) {
        return fetchNeighbours(node).then(function(bioEntities){
            // console.log("bioentities"); console.log(JSON.parse(JSON.stringify(bioEntities)));
            bioEntities.forEach(function(d, i){
                bioEntities[i].bioEntityIds = [deepCopy(d)];
            });

            /*
            Group entities, e.g. when there are multiple occurencies of a species in the same compartment with
            the same role, complex or whatever grouping conditions are specified.
             */
            // console.log("nom-grouped bioentities"); console.log(JSON.parse(JSON.stringify(bioEntities)));
            groupBioEntites(bioEntities, ["name", "_compartmentId", "_complexId", /*"role"*/"reactionType"]);
            // console.log("grouped bioentities"); console.log(JSON.parse(JSON.stringify(bioEntities)));

            /*
             Filter out entities which have already been encountered.
             */
            //Get object on path to root
            var rootPath = [node.data];
            var nodeAux = node;
            while (nodeAux.parent) {
                nodeAux = nodeAux.parent;
                rootPath.push(nodeAux.data);
            }
            // console.log("bioentities before rootPath filtration", JSON.parse(JSON.stringify(bioEntities)));
            // console.log("rootPath", rootPath);
            // console.log("PATH_EQUALITY_ATTRS", PATH_EQUALITY_ATTRS);
            filterBioEntites(bioEntities, rootPath, PATH_EQUALITY_ATTRS);
            log("filtered bioentities", deepCopy(bioEntities));

            log("bioEntities before nesting", deepCopy(bioEntities));
            //Nesting results in deepcopy, so any modifications are made to copies of the map objects
            var nested = d3.nest()
                .key(function (d) {
                    // return d._compartmentId;
                    return d.comparmentName;
                })
                .entries(bioEntities);

            log("nested", deepCopy(nested));

            //Rename the attributes key and values to name and childern (that is what d3.hieararchy, and mainly the rest
            //of the application, is using (historical reasons).
            function renameNestAttrs(nest) {
                if ("values" in nest) {
                    renameProperty(nest, "key", "name");
                    renameProperty(nest, "values", "children");
                    for (let i = 0; i < nest.children.length; i++) renameNestAttrs(nest.children[i])
                }
            }
            nested.forEach(function(n){renameNestAttrs(n);});

            log("renamed nested", deepCopy(nested));
            // nested = nested
            //     .replace(/"key"/g, '"name"')
            //     .replace(/"values"/g, '"children"');


            function modifyLeafs(x, depth) {
                if(depth === undefined) depth = 0;

                if ("children" in x) {
                    for (let i = 0; i < x.children.length; i++) {
                        modifyLeafs(x.children[i], depth + 1)
                    }
                } else { //TODO
                    // x.name = x.w;
//                        x.name = getNodeNameByGroupByIx(x, ixLastNotNullGroupBy);
                    x.type = x.getType();
                    x.compartment = x._compartmentId;
                    // x.reactionType = "reactionType"; //TODO
                    x.complex1 = node.data._complexId;
                    x.complex2 = x._complexId;
                    x.derived1 = undefined;
                    x.derived2 = undefined;
                    // x.bioEntityIds =
                    //     {id: x.id, _modelId: x._modelId, reaction: x.reaction, x: x.x, y: x.y}];
                    // if (x.complex1 != udnefined || x.complex2 != udnefined) x.name += " (" + x.complex1 + " - " + x.complex2 + ")";
                    // if (x.complex2 != undefined) x.name += " (" + x.complex2 + ")";

                    // delete x.v;
                    // delete x.w;
                    // delete x.attrs;
                }
            }

            for (i = 0; i < nested.length; i++) {
                modifyLeafs(nested[i]);
                //convert to d3. hierarchy
                nested[i] = d3.hierarchy(nested[i])
            }

            ce.nested = nested;
            return nested;
        });
    }

    /*
    Called when children of a node were modified -> depth and height of the D3 hierarchy could have changed
     */
    function updateHierarchy(node) {

        node.depth = node.parent ? node.parent.depth + 1 : 0;
        if (node.children) {
            node.children.forEach(updateHierarchy);
            var heights = node.children.map(function(d) {return d.height;});
            node.height = Math.max.apply(null,heights) + 1;

            // //In case new children were added, the pointers to children data objects need to be actualized
            // node.data.children = node.children.map(function(d) {return d.data} );
        }
        else {
            node.height = 0;
        }
    }

    function nodeMenuClick(code, el) {
        var node = d3.select(el).datum();
        var ixRoot = getParentTreeIx(el);
        new Promise(function(resolve, reject) {
//        if (node.depth > 0)
            if (code != "exp") {
                if (code == "spawn") {
                    if ("bioEntityIds" in node.data) addRoot(node.data.name, ixRoot + 1); //no root path => infinite spawning

                }
                else if (code == "struct") {
                    openSV(node);
                }
                else if (code == "locate") {
                    locateInMap(node);
                }
                else if (code == "locate_subtree") {
                    locateInMap(node, true);
                }
                else if (code == "highlight_path") {
                    highlightPath(el);
                }
                else if (code == "backtrack") {
                    backtrackPath(el);
                }

                else if (code == "despawn") {
                    if (node.depth == 0) {
                        removeRootsFrom(ixRoot);
                        highlightInMap();
                    }
                }
                resolve();
            }
            else {
                new Promise(function(resolve, reject) {

                    if (node.children) {
                        node._children = node.children;
                        delete node.children;
                        // node.children = null;
                        resolve(false);
                    }
                    else if (node._children) {
                        node.children = node._children;
                        /* Since every leaf since level 1 has _children, we have to call expansion in this
                        branch to explore whether the leaves of the just expanded node (which might have been a
                        leaf node) still have children.
                         */
                        // Promise.all(expandNextLevel(node, fetchChildren)).then(function(){resolve();});
                        resolve(false);
                    }
                    else {
                        fetchChildren(node).then(function(ch){
                            //parent of the new childern is null, so it needs to be updated
                            let isLeaf = !(ch && ch.length > 0);
                            if (!isLeaf) {
                                ch.forEach(function(d) {d.parent = node;})
                                node.children = ch;
                            }

                            /*
                             We need also next level in order to be able to visualize whether that is the end of the tree or not.
                             This needs to be done for leaves only since the non-leaves do not need to correspond to proteins.
                             */
                            // Promise.all(expandNextLevel(node, fetchChildren)).then(function(){resolve();});
                            resolve(isLeaf);
                        });
                    }
                }).then(function(isLeaf){
                    if (isLeaf) node.isLeaf = true;
                    updateHierarchy(treeRoots[ixRoot]);
                    updateVisualization(node, ixRoot);
                    if ($("#ceLocateOnExpansion").is(":checked")) locateInMap(node, true);
                    resolve();
                });
            }
        }).then(function() {
            findMatchingNodesInTree(ixRoot);
            d3.select("#ceNodeMenu").classed("hidden", true);
            // removeNodeMenu();
        });
    }

    function clearPaths() {
        $(".ce-path-link").each(function(){d3.select(this).classed("ce-path-link", false);})
        lastBacktrackEl = undefined;
    }

    /*
     For starting node finds path in the respective tree to its root and then iteratively finds nodes in tree i-1
     which spawned root of i and for each of the finds path to the root. For each of the nodes on the path,
     it sets class for the link element in the visualization.
     */
    var lastBacktrackEl;
    function backtrackPath(el) {
        if (el === undefined) el = lastBacktrackEl;
        else lastBacktrackEl = el;

        if (el === undefined  ) return;

        clearPaths();

        if ($(el).length > 0) {
            //the original path start was not removed (hidden)

            let maxDepth = $("#ceBacktrackMaxDepth").val();
            if (maxDepth == "") maxDepth = Number.MAX_VALUE;

            log("maxDepth", maxDepth);

            var ixTree = getParentTreeIx(el);
            for (let i = ixTree; i >= Math.max(ixTree - maxDepth + 1, 0); i--) { //for each tree
                let startNodeElements = [];
                if (i == ixTree) {
                    startNodeElements = [el];
                }
                else { //find trees which spawned root
                    startNodeElements = $("#ceTree" + i + " .ce-spawned").toArray();
                }

                let treeLinks = $("#ceTree" + i + " .ce-link");

                startNodeElements.forEach(function(startNodeElement){
                    let path = getD3Datum(startNodeElement).path(treeRoots[i]);
                    for (let j = 0; j < path.length-1; j++) {
                        treeLinks.each(function(){
                            let linkDatum = getD3Datum(this);
                            if (linkDatum.target == path[j] && linkDatum.source == path[j+1]) {
                                d3.select(this).classed("ce-path-link", true);
                            }
                        })
                    }
                })
            }
        }

        highlightInMap();
    }

    function getD3Datum(e) {
        return e.__data__;
        //return d3.select(e).datum();
    }

    // function expandNextLevel(node, f, depth) {
    //     if(depth === undefined) depth = 0;
    //
    //     let promises = [];
    //     if (depth == 0 || !("id" in node)) {
    //         // If depth == 0, we called the functio for the first time and need to descend. The same holds when
    //         // the node is an inner node.
    //         assert("children" in node || "_children" in node);
    //         /*
    //         In case we are expanding a node which already has children present but some of them have been collapsed,
    //         then these have _children but not children. E.g. exapnsion of a root and then collapsing category,
    //         collapsing the root and expanding it again.
    //          */
    //         let auxChildern = "children" in node ? node.children : node._children;
    //         for (let i = 0; i < auxChildern.length; i++) {
    //             promises = promises.concat(expandNextLevel(auxChildern[i], f, depth + 1))
    //         }
    //     }
    //     else {
    //
    //         if (!nodeHasChildren(node)) {
    //
    //             return [
    //                 f(node).then(function (ch) {
    //                     node._children = ch;
    //                 })];
    //         }
    //     }
    //     return promises;
    // }

    function nodeHasVisibleChildren(node) {
        return ("children" in node && node["children"].length > 0) ? true : false;
    }

    function nodeHasChildren(node) {
        return (("_children" in node && node["_children"].length > 0) || nodeHasVisibleChildren(node)) ? true : false;
    }

    function removeNodeMenu() {

        d3.selectAll("g.ce-node circle")
            .attr("r", function () {
                _r = d3.select(this).attr("_r");
                if (_r != null) return _r;
                else return d3.select(this).attr("r");
            });
        d3.selectAll("g.ce-node text")
            .attr("text-anchor", function () {
                _ta = d3.select(this).attr("_textAnchor");
                if (_ta != null) return _ta;
                else return d3.select(this).attr("text-anchor");
            });
        d3.selectAll(".ce-node-menu").remove();
    }

    function repositionTrees(ixTree) {
        cntTrees = $(".ce-tree").length;
        if (ixTree >= cntTrees) return;

        dist = getTreePosRight(ixTree - 1) - getTreePosLeft(ixTree);

        for (ix = ixTree; ix < cntTrees; ix++) {
            newLeft = getTreePosLeft(ix) + dist;
            d3.select("#ceTree" + parseInt(ix))
                .transition()
                .duration(duration)
                .attr("transform", "translate(" + parseInt(newLeft) + ",0)");
        }
    }

    function getSelectedTypes() {
        selectedTypes = [];
//        d3.select("#selSpeciesType").selectAll("option").filter(function (d, ix1) { return this.selected;}).map(function(d,ix1){return d3.select(this)});
//        d3.selectAll("input:checked").each(function(){selectedTypes.push(d3.select(this).datum());});
        d3.select("#ceSelSpeciesType").selectAll("option").filter(function (d, i) {
            return this.selected;
        })
            .each(function () {
                selectedTypes.push(d3.select(this).datum());
            });
        return selectedTypes;
    }

    // function getExplorationType() {
    //     return d3.select("#ceExplorationTypeSelection input:checked").attr("id")
    // }

    // function getSelectedSpecies() {
    //     e = document.getElementById("ceSelProtein");
    //     return e.options[e.selectedIndex].text;
    // }
    //
    // function speciesTypesUpdated() {
    //
    //     selectedSpecies = $("#ceSelProtein option:selected").text();
    //     d3.select("#ceSelProtein").selectAll("option").remove();
    //
    //     sts = getSelectedTypes();
    //     nds = graph.nodes().filter(function (d) {
    //         return sts.includes(graph.node(d).type)
    //     }).sort(sortAlphabetic);
    //     nds.unshift('SNCA');
    //     d3.select("#ceSelProtein").selectAll("option").data(nds)
    //         .enter()
    //         .append("option")
    //         .text(function (d) {
    //             return d;
    //         })
    //
    //     $("#ceSelProtein option").filter(function () {
    //         return $(this).text() == selectedSpecies;
    //     }).prop('selected', true);
    // }
    //
    // function groupByUpdated(ix) {
    //     groupByOptions = $.extend([], GROUP_BY_TYPES);
    //
    //     if (!ix) {
    //         d3.select("#ceSelGroupBy1").selectAll("option").data(groupByOptions).enter()
    //             .append('option')
    //             .text(function (d) {
    //                 return d.capitalize();
    //             });
    //         groupByUpdated(1);
    //     }
    //     else if (ix <= 2) {
    //         for (ix1 = 1; ix1 <= ix; ix1++) {
    //             aux = $("#ceSelGroupBy" + parseInt(ix1) + " :selected").text().toLowerCase();
    //
    //             groupByOptions.splice(groupByOptions.indexOf(aux), 1)
    //         }
    //         for (ix1 = ix + 1; ix1 <= 3; ix1++) d3.select("#ceSelGroupBy" + parseInt(ix1)).selectAll("option").remove()
    //         d3.select("#ceSelGroupBy" + parseInt(ix + 1)).selectAll("option").data([""].concat(groupByOptions)).enter()
    //             .append('option')
    //             .text(function (d) {
    //                 return d.capitalize();
    //             })
    //     }
    // }

    function cleanLegends() {
        compartmentTypes = [];
        reactionTypes = [];
        d3.select("#ceLegendCompartmentsColors").selectAll("div").remove();
        d3.select("#ceLegendReactionsColors").selectAll("div").remove();
    }

    function focusOnRoot(ixRoot) {
        focusSvgOnPosition([getTreePosLeft(ixRoot), 0]);
        locateInMap(treeRoots[ixRoot]);
    }

    function updateLegends() {
        let divLegend = $("#ceLegend")
        divLegend.empty();
        let divLegendTrees = $('<div class="ce-legend-trees"></div>').appendTo(divLegend);
        let divLegendTreesRow = $('<div class="ce-legend-trees-row"></div>').appendTo(divLegendTrees);
        for (let i=0; i < treeRoots.length; i++) {
            let divTree = $("<div></div>").appendTo(divLegendTreesRow);
            divTree.attr("id", function() {return "ceLegendTreeRootname" + i ;});
            divTree.attr("class", "ce-legend-tree-label ce-legend-tree-cell");
            divTree.css("background-color", function () { return getColorForTreeIx(i); });
            divTree.text(function () { return treeRoots[i].data.name; });
            divTree.on("click", function(){ focusOnRoot(i); });
        }
        // divLegendTreesRow = $('<div class="ce-legend-trees-row"></div>').appendTo(divLegendTrees);
        // for (let i=0; i < treeRoots.length; i++) {
        //     let x =
        //         '<div class="form-group">' +
        //         ' <label for="usr">Name:</label>' +
        //         '<input type="text" class="form-control" id="usr">' +
        //         '</div>';
        //     let divTree = $(x).appendTo(divLegendTreesRow);
        //     divTree.addClass("ce-legend-tree-cell");
        //     // divTree.text("abc");
        // }
    }

    function zoomTransformX(pos) {
        log("zoomTransformX");
        return d3.zoomTransform($("#ceSvg > g")[0]).applyX(pos);
    }

    function zoomScale(x) {
        return x * d3.zoomTransform($("#ceSvg > g")[0]).k;
    }

    /*
     Returns tree right coordinate relative to the SVG element, including the zoom transformation.
     */
    function getTreePosRight(ixTree) {
        var tree = $("#ceTree" + parseInt(ixTree));
        var pos = tree.length ? getTreePosLeft(ixTree) + tree[0].getBBox().width : 0;
        assert(typeof pos === "number");
        return pos;
    }
    ce.getTreePosRight = getTreePosRight;

    /*
        Returns tree left coordinate relative to the SVG element, including the zoom transformation.
         */
    function getTreePosLeft(ixTree) {
        var pos = 0;
        for (let i = 0; i < ixTree; i ++) pos += $("#ceTree" + parseInt(i))[0].getBBox().width + consts.treeMarginRight;
        return pos;


        // var tree = $("#ceTree" + parseInt(ixTree));
        // var  pos = tree.length ? tree.offset().left - $("#ceSvg").offset().left : 0
        // // pos = zoomTransformX(pos);
        // console.log("pos",pos);
        // assert(typeof pos === "number");
        // return pos;
    }
    ce.getTreePosLeft = getTreePosLeft;

    function resetSvgPosition(redraw) {
        if(redraw === undefined) redraw = true;
        d3.select("#ceSvg > g").call(zoom.transform, d3.zoomIdentity); //for some reason when using svgG, instead of the d3 selection, the transform of the g element does not get updated!
    }

    function focusSvgOnPosition(coords) {
        d3.select("#ceSvg > g").transition().duration(duration)
            .call(zoom.transform, d3.zoomIdentity.translate(-coords[0], -coords[1]));
    }

    ce.focusSvgOnPosition = focusSvgOnPosition;

    ce.reinitializeVisualization = function reinitializeVisualization() {
        removeRootsFrom(0);
        $("#ceTree").remove();
        highlightInMap();
    }

    function closeModal() {
        $("#ceModal").modal("hide");
    }

    function openModal(params) {

        let modal = $("#ceModal");
        let modalContent = modal.find(".modal-content");
        modalContent.empty();
        let modalContentClass = "modal-content";
        if ("modalClass" in params) modalContentClass += " " + params.modalClass;
        modalContent.attr("class", modalContentClass);
        if ("header" in params) {
            let header = $('<div class="modal-header panel-heading"></div>').appendTo(modalContent);
            header.append('<button type="button" class="close">&times;</button>');
            header.append('<h4>' + params.header + '</h4>');

        }
        if ("body" in params) {
            let body = $('<div class="modal-body"></div>').appendTo(modalContent);
            body.append(params.body);
        }
        // '           <div class="modal-footer">' +
        // '               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        // '           </div>' +

        modal.modal("show");
    }

    ce.export = function () {
        openModal({
            body:
            '<button type="button" class="btn btn-block" onclick="ce.exportToFormat(\'csv\')">CSV</button>' +
            '<button type="button" class="btn btn-block" onclick="ce.exportToFormat(\'json\')">JSON</button>'
        })
    }

    ce.exportToFormat = function (format) {
        let exportString;
        if (format === "csv") exportString = exportTrees2CSV();
        if (format === "json") exportString = exportTrees2JSON();

        // console.log(exportString)

        let fileName = 'export.' + format;
        let blob = new Blob([exportString], { type: 'text/' + format + ';charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, fileName);
        } else {
            let link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", fileName);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }

        closeModal();
    }

    function exportTrees2JSON(){
        let trees = [];
        for (let i = 0; i < treeRoots.length; i++) {
            trees.push(exportTree2JSON(treeRoots[i]));
        }
        return JSON.stringify(trees, null, 2);
    }

    function exportTree2JSON(node){
        let rv = {};
        rv.name = node.data.name;

        if ("bioEntityIds" in node.data) {
            let bioEntity = node.data.bioEntityIds[0];
            rv.element_type = bioEntity.getType();
            if ("reaction" in bioEntity) {
                rv.reaction_type = bioEntity.reaction.getType();
                rv.role_in_reaction = bioEntity.role;
            }
        }
        else {
            rv.element_type = consts.compartmentTypeName;
        }
        if ("children" in node) {
            rv.children = [];
            for (let i = 0; i < node.children.length; i++) {
                rv.children.push(exportTree2JSON(node.children[i]));
            }
        }

        return rv;
    }

    function exportTrees2CSV(){
        let csvString = "";
        for (let i = 0; i < treeRoots.length; i++) {
            csvString += exportTree2CSV(treeRoots[i]);
            if (i < treeRoots.length -1) csvString += "\n\n";
        }
        return csvString;
    }

    function exportTree2CSV(node, rows, row) {
        if (rows === undefined) rows = [];
        if (row === undefined) row = [];

        if ("bioEntityIds" in node.data) {
            let nodeString = node.data.name;
            let bioEntity = node.data.bioEntityIds[0];
            nodeString += "[" + bioEntity.getType() + "]";
            if ("reaction" in bioEntity) {
                row.push(bioEntity.reaction.getType());
                nodeString += "[" + node.parent.data.name + "]"; //compartment name
                nodeString += "[" + bioEntity.role + "]";
            }
            row.push(nodeString);
        }

        if (node.children){
            for (let i = 0; i < node.children.length; i++) {
                exportTree2CSV(node.children[i], rows, deepCopy(row));
            }
        }

        if (node.height == 0) {
            rows.push(row);
        }
        if (node.depth == 0) {
            return rows.join('\n');
            // let csvRows = [];
            // for (let i = 0; i < rows.length; ++i) {
            //     for (let j = 0; j < rows[i].length; ++j) {
            //         rows[i][j] = '\"' + rows[i][j] + '\"'; //TODO handle commas
            //     }
            //     csvRows.push(rows[i].join(','));
            // }
            // return csvRows.join('\r\n');
        }
    }

//Dragging and zooming behavior
    function zoomed() {
        svgG.attr("transform", d3.event.transform);

        // svgG.attr("transform", d3.zoomTransform(this)); //calls toString of d3 transformation
    }

    function isComplex(bioEntity) {
        return bioEntity.getType().toLowerCase() == "complex";
    }

    /*********************
     Structure viewer
     *********************/

    function queryPDBRecordsByUniprotIds(uniprotIds) {
        let pdbRecords = {};
        return $.ajax({
            type: "POST",
            url: "https://www.ebi.ac.uk/pdbe/api/mappings/best_structures/",
            data: uniprotIds.join(",")
        });
    }

    function getUniprotIds(data){
        ids = [];
        data.bioEntityIds.forEach(function(bioEntity) {
            bioEntity.getReferences().forEach(function(reference){
                if (reference.getType().toLowerCase() == "uniprot") ids.push(reference.getResource());
            });
        });
        uniquify(ids);
        return ids;
    }

    /*
    Uses EBI REST calls based on SIFT mappings - https://www.ebi.ac.uk/pdbe/api/doc/sifts.html
     */
    function openSV(node) {
        if (PDB_MAPPING_FROM_MINERVA) {

            //https://stackoverflow.com/questions/37984216/bootstrap-accordion-with-arrows
            $("#ceStructureViewer .ce-records-list > *").remove();

            var bioEntity = node.data.bioEntityIds[0];
            log("uniprotIds structures", bioEntity.getOther().structures);
            var structures = bioEntity.getOther().structures;
            Object.keys(structures).forEach(function(k){
                structures[k].sort(function(a,b){return b.coverage - a.coverage;})
            });
            var recordsListSel = "#ceStructureViewer .ce-records-list";
            var recordsList = $(recordsListSel);
            recordsList = $('<div class="panel-group" id="ceAccordion"></div>').appendTo(recordsList);
            for (uniprotId in structures) {
                var molRecord = $('<div class="ce-molecule-record panel panel-default"></div>').appendTo(recordsList);
                var molheader = $('<div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#ceAccordion" data-target="#collapse'+uniprotId+'"></div>')
                    .appendTo(molRecord);
                $('<h4 class="panel-title">' + uniprotId + '</h4>').appendTo(molheader);
                var molContentPanel = $('<div id="collapse' + uniprotId + '" class="panel-collapse collapse">').appendTo(molRecord);
                var molContentPanelBody = $('<div class="panel-body">').appendTo(molContentPanel);
                // var uniprotIdDiv = $('<div class="ce-uniprot-name">' + uniprotId  + '</div>').appendTo(molRecord);
                // uniprotIdDiv.addClass("arrow-down");
                // uniprotIdDiv.on("click", uniprotIdOnClick);
                var molStructures = $('<div class="ce-molecule-structures"></div>').appendTo(molContentPanelBody);

                structures[uniprotId].forEach(function(pdbRecord) {
                    var pdbRecordContainer = $('<div class="container-fluid ce-pdb-record-info"></div>').appendTo(molStructures);
                    pdbRecordContainer.on("click", pdbIdSelected);
                    // pdbRecordContainer.data("ce-pdb-record", pdbRecordContainer);
                    d3.select(".ce-pdb-record-info:last-child").data([pdbRecord]); //assign the whole pdb record for easier manipulation later
                    [
                        ["pdbId", "PDB ID"],
                        ["chainId", "Chain"],
                        ["coverage", "Coverage"],
                        ["structStart", "Start"],
                        ["structEnd", "End"],
                        ["experimentalMethod", "Experimental method"],
                    ].forEach(function(d) {
                        var row = $("<div></div>").addClass("row");
                        $("<div></div>")
                            .addClass("col-md-6 ce-pdb-record-info-attribute")
                            .text(d[1])
                            .appendTo(row);
                        $("<div></div>")
                            .addClass("col-md-6 ce-pdb-record-info-value")
                            .text(pdbRecord[d[0]])
                            .appendTo(row);
                        pdbRecordContainer.append(row);
                    });
                    var row = $("<div></div>").addClass("row");
                    row.append($('<div class="col-md-12 ce-pdb-record-info-link"><a href="https://www.ebi.ac.uk/pdbe/entry/pdb/' + pdbRecord.pdbId + '" target="_blank">PDB</a></div>'));
                    pdbRecordContainer.append(row);
                });

                $(".ce-pdb-record-info-link a").click(function(e) {
                    //do something
                    e.stopPropagation();
                });

                LiteMol.initializePlugin();

                $("#ceStructureViewer").css("width", "100%");
                // $("#ceLitemolApp").css("width", "");
                $("#ceLitemolApp").css("height", "100%");
            }
        }
        else {
            uniprotIds = getUniprotIds(node.data);
            log("uniprotIds", uniprotIds);
            queryPDBRecordsByUniprotIds(uniprotIds).then(function (uniprotIdsPdbs) {
                log("uniprotIdsPdbs", uniprotIdsPdbs);
                pdbRecords = uniprotIdsPdbs[uniprotIds[0]];
                log("pdbRecords", pdbRecords);

                $("#ceStructureViewer .ce-records-list > *").remove();

                // $("<div></div>").addClass("ce-pdb-records-info").appendTo($("#ceStructureViewer"));
                pdbRecords.forEach(function(pdbRecord){
                    //for each PDB record create bootstrap container which will have in rows the PDB record attributes

                    $("#ceStructureViewer .ce-records-list").append('<div class="container-fluid ce-pdb-record-info"></div>');
                    var pdbRecordContainer = $(".ce-pdb-record-info:last-child");
                    pdbRecordContainer.on("click", {pdbId: "pdb_id", chainId: "chain_id", structStart: "start", structEnd: "end"}, pdbIdSelected);
                    // pdbRecordContainer.data("ce-pdb-record", pdbRecordContainer);
                    d3.select(".ce-pdb-record-info:last-child").data([pdbRecord]); //assign the whole pdb record for easier manipulation later

                    [
                        ["pdb_id", "PDB ID"],
                        ["chain_id", "Chain"],
                        ["coverage", "Coverage"],
                        ["start", "Start"],
                        ["end", "End"],
                        ["experimental_method", "Experimental method"],
                    ].forEach(function(d) {
                        var row = $("<div></div>").addClass("row");
                        $("<div></div>")
                            .addClass("col-md-6 ce-pdb-record-info-attribute")
                            .text(d[1])
                            .appendTo(row);
                        $("<div></div>")
                            .addClass("col-md-6 ce-pdb-record-info-value")
                            .text(pdbRecord[d[0]])
                            .appendTo(row);
                        pdbRecordContainer.append(row);
                    });
                    var row = $("<div></div>").addClass("row");
                    row.append($('<div class="col-md-12 ce-pdb-record-info-link"><a href="https://www.ebi.ac.uk/pdbe/entry/pdb/' + pdbRecord.pdb_id + '" target="_blank">PDB</a></div>'));
                    pdbRecordContainer.append(row);
                });

                $(".ce-pdb-record-info-link a").click(function(e) {
                    //do something
                    e.stopPropagation();
                })


                LiteMol.initializePlugin();

                $("#ceStructureViewer").css("width", "100%");
                // $("#ceLitemolApp").css("width", "");
                $("#ceLitemolApp").css("height", "100%");
            }, function(reason) {
                log(reason);
                openModal({
                    header: "Structure not available",
                    modalClass: "panel-danger",
                    body: '<div>No structure for Uniprot ID(s) "' + uniprotIds.join(",") + '" available in PDBe SIFTS.</div>' +
                    '<div class="text-muted" style="text-align: right">REST API error code ' + reason.status + ' (' + reason.statusText + ')</div>'
                });
            });
        }
    }

    ce.closeSV = function closeSV() {
        $("#ceStructureViewer").css("width", "0");
    }

    function pdbIdSelected(e) {
        var params = e.data; //data added to event by jquery onclick function
        log("pdbIdSelect params", params);
        if (params === undefined) params = {pdbId: "pdbId", chainId: "chainId", structStart: "structStart", structEnd: "structEnd"};
        var pdbRecord = getD3Datum(this);
        log("pdbRecord", pdbRecord, "pdbId", params.pdbId, "pdbRecord[pdbId]", pdbRecord[params.pdbId]);
        liteMolPlugin.clear();
        liteMolPlugin.loadMoleculeCustom(pdbRecord[params.pdbId]).then(function () {
            liteMolPlugin.highlight(pdbRecord[params.chainId], pdbRecord[params.structStart], pdbRecord[params.structEnd]);
        })

    }

    // function uniprotIdOnClick(e) {
    //     var structs = $(this).parent().find(".ce-molecule-structures");
    //     console.log("structs", structs);
    //
    // }

    /*********************
     Common
     *********************/

    String.prototype.capitalize = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    function sortAlphabetic(a, b) {
        aname = a.toLowerCase();
        bname = b.toLowerCase();
        if (aname < bname) return -1;
        if (aname > bname) return 1;
        return 0;
    }

    function isString(obj) {
        return (Object.prototype.toString.call(obj) === '[object String]');
    }

    function assert(condition, message) {
        if (!condition) {
            message = message || "Assertion failed";
            if (typeof Error !== "undefined") {
                throw new Error(message);
            }
            throw message; // Fallback
        }
    }

    /*
    Tests equality of two object with respect to given array of keys. If no are given, all common keys are tested.
     If the keys are compound objects and descend is set to true, they are also tested for equality,
     but only on common attributes, i.e. keys are not taken into account in such case.
     */
        function objectsEquality(o1, o2, keys, descend) {
            if(descend === undefined) descend = true;

            if (keys === undefined) {
                keys = Object.keys(o1).filter(function (d) {
                    return new Set(Object.keys(o2)).has(d);
                });
            }
            for (let k = 0; k < keys.length; k++) {
                if (typeof(o1[keys[k]]) === "object" &&  typeof(o2[keys[k]]) === "object") {
                    if (o1[keys[k]] === null || o2[keys[k]] === null) {
                        //nulls are also objects
                        if (o1[keys[k]] !== null || o2[keys[k]] !== null) return false;
                    }
                    else if (descend && !objectsEquality(o1[keys[k]], o2[keys[k]])) return false;
                }
                else if (typeof(o1[keys[k]]) != typeof(o2[keys[k]])) return false;
                else if (o1[keys[k]] != o2[keys[k]]) return false;

            }
            return true;
        }
    /*
    objectsEquality({a:1},{a:1}) -> True
    objectsEquality({a:1, b:1},{a:1}) -> True
    objectsEquality({a:1, b:1},{a:1, b:2}) -> false
    objectsEquality({a:1, b:1},{a:1, b:2}, ["a"]) -> True
    objectsEquality({a:1, b:1},{a:1, b:2}, ["a", "b"]) -> false
     objectsEquality({a:1, b:1, c:1},{a:1, b:2}, ["a", "c"]) -> false
     objectsEquality({a:1, b:1, c:null},{a:1, b:2}, ["a", "c"]) -> false
     objectsEquality({a:1, b:1, c:null},{a:1, b:2, c:null}, ["a", "c"]) -> true //ALTHOUGH NULL==NULL shoould lead to NULL, not true
     objectsEquality({a:1, b:1, c:null},{a:1, b:2, c:7}, ["a", "c"]) -> false
     objectsEquality({a:1, b:1, c:null},{a:1, b:1, c:7}, ["a", "b"]) -> true
     objectsEquality({a:1, b:1, c:null, d:{a:1, b:2}},{a:1, b:1, c:7, d:{a:1, b:2}}, ["a", "b", "d"]) -> true
     objectsEquality({a:1, b:1, c:null, d:{a:1, b:2}},{a:1, b:1, c:7, d:{a:1, b:3}}, ["a", "b", "d"]) -> false
     objectsEquality({a:1, b:1, c:null, d:{a:1, b:2}},{a:1, b:1, c:7, d:{a:1, b:3}}, ["a", "b", "d"], false) -> true
    */

    /*
    Removes duplicities from an array. Elements are duplicit if the agree in values of keys
     (the can disagree in the remaining values). If such a situation occurs, only the first of the occurrences is kept.
     */
    function uniquify (arr, keys) {
        for (let i = arr.length-1; i>=0; i--) {
            for (let j = i -1 ; j >= 0; j--) {
                // if (JSON.stringify(arr[i]) == JSON.stringify(arr[j])) {
                if (objectsEquality(arr[i], arr[j], keys)) {
                    arr.splice(i, 1);
                    break;
                }
            }
        }
    }
    // a=[{a:1, b:2, c:3}, {a:1, b:2, c:4}, {a:2, b:2, c:3}, {a:1, b:2, c:4}]; uniquify(a, ["a","b"]); a
    // a=[{a:1, b:2, c:3}, {a:1, b:2, c:4}, {a:2, b:2, c:3}, {a:1, b:2, c:4}]; uniquify(a, ["a","b", "c"]); a
    // a=[{a:1, b:2, c:3}, {a:1, b:2, c:4}, {a:2, b:2, c:3}, {a:1, b:2, c:4}]; uniquify(a, ["b"]); a
    // a=[{a:1, b:{b1:1, b1:2}, c:3}, {a:1, b:{b1:1, b1:2}, c:4}, {a:2, b:{b1:1, b1:2}, c:3}, {a:1, b:{b1:1, b1:2}, c:4}]; uniquify(a, ["b"]); a -> 1 object
    // a=[{a:1, b:{b1:1, b1:2}, c:3}, {a:1, b:{b1:1, b1:2}, c:4}, {a:2, b:{b1:1, b1:2}, c:3}, {a:1, b:{b1:1, b1:3}, c:4}]; uniquify(a, ["b"]); a -> 2 object

    function objectInArray(o, arr, keys) {
        for (let i = 0; i < arr.length; i++)
            if (objectsEquality(o, arr[i], keys)) return i;
        return -1;
    }
    // objectInArray({a:1,b:2,c:3, d:4}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}], ["a"])
    // objectInArray({a:1,b:2,c:3, d:4}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}], ["d"])
    // objectInArray({a:1,b:2,c:3, d:4}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}], ["a", "b"])
    // objectInArray({a:1,b:2,c:3, d:{e:1, b:2}}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}, {a:3, d:{e:1, b:2}}], ["d"]) -> 3
    //objectInArray({a:1,b:2,c:3, d:{e:1, b:2}}, [{a:1, c:3}, {a:1, b:2, c:3}, {a:1, b:3, c:3}, {a:3, d:{e:1, b:3}}], ["d"]) -> -1

    ce.toggler = function (id) {
        $("#" + id).toggle();
    };

    ce.setHighlightBySurface = function(state) {
        settings.neighboursBySurface = state; //true or false
    };

    function roughSizeOfObject(object) {

        let objectList = [];
        let stack = [object];
        let bytes = 0;

        while (stack.length) {
            let value = stack.pop();

            if (typeof value === 'boolean') {
                bytes += 4;
            }
            else if (typeof value === 'string') {
                bytes += value.length * 2;
            }
            else if (typeof value === 'number') {
                bytes += 8;
            }
            else if
            (
                typeof value === 'object'
                && objectList.indexOf(value) === -1
            ) {
                objectList.push(value);

                for (let i in value) {
                    stack.push(value[i]);
                }
            }
        }
        return bytes;
    }

    /*
    Deepcopy of object, including methods. The copy of methods is limited only to hierarchy formed by arrays (not sets, for example).
     */
    function deepCopy(source) {
        var target;
        if (typeof source != "object") return source;


        if (Object.prototype.toString.call(source) == "[object Array]") {
            target = [];
            for (let i=0; i < source.length; i++){
                typeof source[i] == "object" ? target.push(deepCopy(source[i])) : target.push(source[i]);
            }
            return target;
        }
        else {
            target = Object.create(source.__proto__);
            for (let j =  0; j < Object.keys(source).length; j++){
                let k = Object.keys(source)[j];

                if (Object.prototype.toString.call(source[k]) == "[object Array]") {
                    target[k] = []
                    for (let i=0; i < source[k].length; i++) {
                        typeof source[k][i] == "object" ? target[k].push(deepCopy(source[k][i])) : target[k].push(source[k][i]);
                    }
                }
                else if (Object.prototype.toString.call(source[k]) == "[object Object]") {
                    target[k] = deepCopy(source[k]);
                }
                else {
                    target[k] = source[k];
                }
            }

            return target;
        }
    }

    function renameProperty(o, old_key, new_key) {
        if (old_key !== new_key) {
            Object.defineProperty(o, new_key,
                Object.getOwnPropertyDescriptor(o, old_key));
            delete o[old_key];
        }
    }

    function log() {
        if (LOG == 1) console.log.apply(null, arguments)
    }

    // function clone(src) {
    //     function mixin(dest, source, copyFunc) {
    //         var name, s, i, empty = {};
    //         for(name in source){
    //             // the (!(name in empty) || empty[name] !== s) condition avoids copying properties in "source"
    //             // inherited from Object.prototype.	 For example, if dest has a custom toString() method,
    //             // don't overwrite it with the toString() method that source inherited from Object.prototype
    //             s = source[name];
    //             if(!(name in dest) || (dest[name] !== s && (!(name in empty) || empty[name] !== s))){
    //                 dest[name] = copyFunc ? copyFunc(s) : s;
    //             }
    //         }
    //         return dest;
    //     }
    //
    //     if(!src || typeof src != "object" || Object.prototype.toString.call(src) === "[object Function]"){
    //         // null, undefined, any non-object, or function
    //         return src;	// anything
    //     }
    //     if(src.nodeType && "cloneNode" in src){
    //         // DOM Node
    //         return src.cloneNode(true); // Node
    //     }
    //     if(src instanceof Date){
    //         // Date
    //         return new Date(src.getTime());	// Date
    //     }
    //     if(src instanceof RegExp){
    //         // RegExp
    //         return new RegExp(src);   // RegExp
    //     }
    //     var r, i, l;
    //     if(src instanceof Array){
    //         // array
    //         r = [];
    //         for(i = 0, l = src.length; i < l; ++i){
    //             if(i in src){
    //                 r.push(clone(src[i]));
    //             }
    //         }
    //         // we don't clone functions for performance reasons
    //         //		}else if(d.isFunction(src)){
    //         //			// function
    //         //			r = function(){ return src.apply(this, arguments); };
    //     }else{
    //         // generic objects
    //         r = src.constructor ? new src.constructor() : {};
    //     }
    //     return mixin(r, src, clone);
    //
    // }
    //o1 = {a:1, b:2}; o2 = deepCopy(o1); o2.a = 5; o1; o2;

}( window.ce = window.ce || {}, jQuery ));
