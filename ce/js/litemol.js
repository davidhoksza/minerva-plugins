/*
 * Copyright (c) 2016 - now David Sehnal, licensed under Apache 2.0, See LICENSE file for more info.
 */
// var LiteMol;
var liteMolPlugin;
(function (LiteMol) {
    var controller;
    (function (controller) {

                var Plugin = LiteMol.Plugin;
                var Views = Plugin.Views;
                var Bootstrap = LiteMol.Bootstrap;
                // everything same as before, only the namespace changed.
                var Query = LiteMol.Core.Structure.Query;
                // You can look at what transforms are available in Bootstrap/Entity/Transformer
                // They are well described there and params are given as interfaces.
                var Transformer = Bootstrap.Entity.Transformer;
                var Tree = Bootstrap.Tree;
                var Transform = Tree.Transform;
                var LayoutRegion = Bootstrap.Components.LayoutRegion;
                var CoreVis = LiteMol.Visualization;
                var Visualization = Bootstrap.Visualization;
                // all commands and events can be found in Bootstrap/Event folder.
                // easy to follow the types and parameters in VSCode.
                // you can subsribe to any command or event using <Event/Command>.getStream(plugin.context).subscribe(e => ....)
                var Command = Bootstrap.Command;
                var Event = Bootstrap.Event;


                function applyTransforms(actions) {
                    return liteMolPlugin.applyTransform(actions);
                }

                function createSelectionTheme(color) {
                    // for more options also see Bootstrap/Visualization/Molecule/Theme
                    var colors = LiteMol.Core.Utils.FastMap.create();
                    colors.set('Uniform', CoreVis.Color.fromHex(0xffffff));
                    colors.set('Selection', color);
                    colors.set('Highlight', CoreVis.Theme.Default.HighlightColor);
                    return Visualization.Molecule.uniformThemeProvider(void 0, { colors: colors });
                }

                function selectNodes(what) {
                    return liteMolPlugin.context.select(what);
                }

        LiteMol.initializePlugin = function () {
            if (!liteMolPlugin) {
            //     liteMolPlugin.destroy(); liteMolPlugin = undefined;
            //
            // }else {
                liteMolPlugin = LiteMol.Plugin.create({
                    target: '#ceLitemolApp',
                    viewportBackground: '#000000',
                    layoutState: {
                        collapsedControlsLayout: LiteMol.Bootstrap.Components.CollapsedControlsLayout.Landscape,
                        hideControls: false,
                        isExpanded: false
                    },
                    // Knowing how often and how people use LiteMol
                    // gives us the motivation and data to futher improve it.
                    //
                    // This option is OFF by default!
                    allowAnalytics: false
                });

                liteMolPlugin.loadMoleculeCustom = function(pdbId) {
                    // this builds the transforms needed to create a molecule
                    var action = Transform.build()
                        .add(liteMolPlugin.context.tree.root, Transformer.Data.Download, { url: "https://www.ebi.ac.uk/pdbe/static/entry/" + pdbId + "_updated.cif", type: 'String', id: pdbId })
                        .then(Transformer.Data.ParseCif, { id: pdbId }, { isBinding: true })
                        .then(Transformer.Molecule.CreateFromMmCif, { blockIndex: 0 }, { isBinding: true })
                        .then(Transformer.Molecule.CreateModel, { modelIndex: 0 }, { isBinding: false, ref: 'model' })
                        .then(Transformer.Molecule.CreateMacromoleculeVisual, { polymer: true, polymerRef: 'polymer-visual', het: true, water: true });
                    return applyTransforms(action);
                    //.then(() => nextAction())
                }

                liteMolPlugin.highlight = function(chainId, startResidueNumber, endResidueNumber){
                    var visual = selectNodes('polymer-visual')[0];
                    if (!visual) return;
                    const authAsymId = chainId;
                    const residues = [];
                    for (let i = startResidueNumber; i < endResidueNumber; i++) {
                        residues.push({ authAsymId, authSeqNumber: i  });
                    }
                    const query = Query.residues.apply(null, residues)/*.compile()*/;
                    // var query = Query.sequence('1', chain, { seqNumber: start }, { seqNumber: end });
                    var theme = createSelectionTheme(CoreVis.Color.fromHex(0x38d24a));
                    var action = Transform.build()
                        .add(visual, Transformer.Molecule.CreateSelectionFromQuery, { query: query, name: 'Covered region' }, { ref: 'covered-region' })
                        // .then(Transformer.Molecule.CreateVisual, { style: Visualization.Molecule.Default.ForType.get('BallsAndSticks') });
                    applyTransforms(action).then(function () {
                        Command.Visual.UpdateBasicTheme.dispatch(liteMolPlugin.context, {visual: visual, theme: theme});
                        Command.Entity.Focus.dispatch(liteMolPlugin.context, selectNodes('covered-region'));
                    });
                }
            }
        }






        // var id = '4y2g';
        // liteMolPlugin.loadMolecule({
        //     id: id,
        //     format: 'cif',
        //     url: "https://www.ebi.ac.uk/pdbe/static/entry/" + id.toLowerCase() + "_updated.cif",
        //     // instead of url, it is possible to use
        //     // data: "string" or ArrayBuffer (for BinaryCIF)
        //     // loaded molecule and model can be accessed after load
        //     // using plugin.context.select(modelRef/moleculeRef)[0],
        //     // for example plugin.context.select('1tqn-molecule')[0]
        //     moleculeRef: id + '-molecule',
        //     modelRef: id + '-model'
        // }).then(function () {
        //     console.log('Molecule loaded');
        // }).catch(function (e) {
        //     console.error(e);
        // });

        // To see all the available methods on the SimpleController,
        // please check src/Plugin/Plugin/SimpleController.ts 
        //////////////////////////////////////////////////////////////
        //
        // The underlaying instance of the plugin can be accessed by 
        //
        //   plugin.instance
        //////////////////////////////////////////////////////////////
        //
        // To create and apply transforms, use
        //
        //   let t = plugin.createTransform();
        //   t.add(...).then(...);
        //   plugin.applyTransform(t);
        // 
        // Creation of transforms is illusted in other examples. 
        //////////////////////////////////////////////////////////////
        //
        // To execute commands, the SimpleController provides the method command.
        // 
        //   plugin.command(command, params);
        // 
        // To find examples of commands, please see the Commands example.
        //////////////////////////////////////////////////////////////
        //
        // To subscribe for events, the SimpleController provides the method subscribe.
        // 
        //   plugin.subscribe(event, callback);
        // 
        // To find examples of events, please see the Commands example as well.
        // It shows how to subscribe interaction events, where available events are located, etc.
    })(controller = LiteMol.controller || (LiteMol.controller = {}));
})(LiteMol || (LiteMol = {}));
